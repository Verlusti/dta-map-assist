#include "dtapathdialog.h"
#include "ui_dtapathdialog.h"

#include "dta.h"

#include <QDir>
#include <QMessageBox>
#include <QFileDialog>

#define FILE_DIALOG_NAME "Select DTA Folder"
#define MSGBOX_REGISTRY_PICK_FAILED "Unable to pick DTA install path from registry."
#define MSGBOX_INVALID_PATH_ON_ACCEPT "Invalid DTA Folder given."

DtaPathDialog::DtaPathDialog(QWidget *parent) : QDialog(parent), ui(new Ui::DtaPathDialog)
{
    ui->setupUi(this);

    connect(ui->buttonSelectDirectory,SIGNAL(clicked(bool)),this,SLOT(onButtonSelectDirectoryClicked()));
    connect(ui->buttonReadFromRegistry,SIGNAL(clicked(bool)),this,SLOT(onButtonReadFromRegistryClicked()));
    connect(ui->editBaseDir,SIGNAL(textChanged(QString)),this,SLOT(updateBaseDirStatus()));
    connect(ui->buttonBox,&QDialogButtonBox::accepted,this,&DtaPathDialog::onAccepted);
    connect(ui->buttonBox,&QDialogButtonBox::rejected,this,&DtaPathDialog::reject);
}

DtaPathDialog::~DtaPathDialog()
{
    delete ui;
}

void DtaPathDialog::showEvent(QShowEvent *ev)
{
    QDialog::showEvent(ev);

    // read from dta class
    ui->editBaseDir->setText(QDir::toNativeSeparators(Dta::baseDir().path()));
}

void DtaPathDialog::updateBaseDirStatus()
{
    ui->labelBaseDirStatus->setText(Dta::baseDirStatusString(ui->editBaseDir->text()));
}

void DtaPathDialog::onButtonSelectDirectoryClicked()
{
    QDir dir(ui->editBaseDir->text());
    if (!dir.isReadable()) dir = QDir::home();
    QString base_dir = QFileDialog::getExistingDirectory(this,QStringLiteral(FILE_DIALOG_NAME),dir.path(),QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    if (base_dir.isEmpty()) return;
    ui->editBaseDir->setText(QDir::toNativeSeparators(base_dir));
}

void DtaPathDialog::onButtonReadFromRegistryClicked()
{
    QString base_dir = Dta::baseDirFromRegistry();
    if (base_dir.isEmpty()) {
        QMessageBox msg_box;
        msg_box.setText(QStringLiteral(MSGBOX_REGISTRY_PICK_FAILED));
        msg_box.exec();
        return;
    }
    ui->editBaseDir->setText(QDir::toNativeSeparators(base_dir));
}

void DtaPathDialog::onAccepted()
{
    if (!Dta::isBaseDirValid(ui->editBaseDir->text())) {
        QMessageBox msg_box;
        msg_box.setText(QStringLiteral(MSGBOX_INVALID_PATH_ON_ACCEPT));
        msg_box.exec();
        return;
    }
    Dta::setBaseDir(ui->editBaseDir->text());
    Dta::writeSettings();
    accept();
}

