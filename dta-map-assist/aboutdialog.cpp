#include "aboutdialog.h"
#include "ui_aboutdialog.h"

#define ICONS8_URL "https://icons8.com/"

AboutDialog::AboutDialog(QWidget *parent) : QDialog(parent), ui(new Ui::AboutDialog)
{
    ui->setupUi(this);

    // program
    ui->labelProgramName->setText(qApp->applicationName());
    ui->labelVersion->setText(qApp->applicationVersion());
    ui->labelAuthor->setText(qApp->organizationName());

    QString icons8_url = QString("<a href=\"%1\">%2</a>").arg(QStringLiteral(ICONS8_URL)).arg(QStringLiteral(ICONS8_URL));
    ui->labelIcons8->setText(icons8_url);
    ui->labelIcons8->setTextFormat(Qt::RichText);
    ui->labelIcons8->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->labelIcons8->setOpenExternalLinks(true);

    QString forum_url = QString("<a href=\"%1\">%2</a>").arg(qApp->organizationDomain()).arg(qApp->organizationDomain());
    ui->labelForum->setText(forum_url);
    ui->labelForum->setTextFormat(Qt::RichText);
    ui->labelForum->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->labelForum->setOpenExternalLinks(true);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
