#include "mainwindow.h"
#include <QApplication>

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setWindowIcon(QIcon(":/icons/world-map-48.png"));

    QCoreApplication::setOrganizationName("Verlusti");
    QCoreApplication::setOrganizationDomain("https://ppmforums.com/viewtopic.php?t=46069");
    QCoreApplication::setApplicationName("DTA Map Assistant");
    QCoreApplication::setApplicationVersion("0.3");

    MainWindow w;
    w.show();

    return a.exec();
}
