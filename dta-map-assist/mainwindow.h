#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "app.h"
#include "aboutdialog.h"

#include <QMainWindow>

class QCommandLineParser;
class QCommandLineOption;
class Editor;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);
    ~MainWindow();

    void updateRecentMaps() { onUpdateRecentMaps(); }

private:
    Ui::MainWindow *ui;    

    App m_App;

    QCommandLineParser* m_Parser;
    QCommandLineOption* m_OptionOpen;
    QCommandLineOption* m_OptionPath;

    AboutDialog* m_AboutDialog;

    void loadMap(const QString& file_name);
    void saveMap();

private slots:
    void init();

    void onLogReady(const QString& logstr, const QtMsgType& type, const QString& type_str, const QString& func_str, const QString& msg);

    void onLoadMapClicked();
    void onExportMapClicked();

    void onCurrentTabChanged(const int& current_index);
    void onTabCloseRequested(const int& index);

    void onUpdateRecentMaps();
    void onRecentMapClicked();

    // TMP
    void onModelDestroyed();

};

#endif // MAINWINDOW_H
