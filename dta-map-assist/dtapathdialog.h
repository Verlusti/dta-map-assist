#ifndef DTAPATHDIALOG_H
#define DTAPATHDIALOG_H

#include <QDialog>

namespace Ui {
class DtaPathDialog;
}

class DtaPathDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DtaPathDialog(QWidget *parent = Q_NULLPTR);
    ~DtaPathDialog();

protected:
      virtual void showEvent(QShowEvent *ev);

private:
    Ui::DtaPathDialog *ui;

private slots:
    void updateBaseDirStatus();
    void onButtonSelectDirectoryClicked();
    void onButtonReadFromRegistryClicked();
    void onAccepted();
};

#endif // DTAPATHDIALOG_H
