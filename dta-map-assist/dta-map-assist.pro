#-------------------------------------------------
#
# Project created by QtCreator 2018-10-28T14:22:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dta-map-assist
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    aboutdialog.cpp \
    app.cpp \
    editor.cpp \
    dtapathdialog.cpp


HEADERS += \
        mainwindow.h \
    aboutdialog.h \
    app.h \
    editor.h \
    dtapathdialog.h

FORMS += \
        mainwindow.ui \
    aboutdialog.ui \
    editor.ui \
    dtapathdialog.ui


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

# dta-map-library
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../dta-map-library/release/ -ldta-map-library
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../dta-map-library/debug/ -ldta-map-library
else:unix: LIBS += -L$$OUT_PWD/../dta-map-library/ -ldta-map-library

INCLUDEPATH += $$PWD/../dta-map-library
DEPENDPATH += $$PWD/../dta-map-library

RESOURCES += \
    resources.qrc

# windows application icon
win32:RC_FILE = dta-map-assist.rc

DISTFILES +=
