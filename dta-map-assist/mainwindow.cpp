#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "log.h"
#include "dta.h"

#include "dtapathdialog.h"
#include "editor.h"

#include <QDebug>
#include <QFileDialog>
#include <QCommandLineParser>
#include <QMessageBox>

#define MSGBOX_DTA_PATH_FAILURE "Unable to determine DTA path. Quit."

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // logging
    Log::installMessageHandler();
    Log::setSystems(Log::ConsoleSystem|Log::InternalSystem|Log::SyslogSystem);
    connect(Log::logger(),SIGNAL(logReady(QString,QtMsgType,QString,QString,QString)),this,SLOT(onLogReady(QString,QtMsgType,QString,QString,QString)));
    connect(ui->buttonClearLog,SIGNAL(clicked(bool)),ui->editLog,SLOT(clear()));

    // command line parameters
    m_Parser = new QCommandLineParser();
    m_OptionOpen = new QCommandLineOption(QStringList() << "o" << "open","Open File","open","");
    m_OptionPath = new QCommandLineOption(QStringList() << "p" << "path","Set DTA path","path","");
    m_Parser->addOption(*m_OptionOpen);
    m_Parser->addOption(*m_OptionPath);
    m_Parser->addHelpOption();
    m_Parser->addVersionOption();
    m_Parser->process(qApp->arguments());

    // DTA Directory
    if (m_Parser->isSet(*m_OptionPath)) {
        Dta::setBaseDir(m_Parser->value(*m_OptionPath),true);
        Dta::writeSettings();
    }
    Dta::readSettings();
    if (!Dta::isBaseDirValid()) {
        DtaPathDialog* dta_path_dlg = new DtaPathDialog();
        connect(dta_path_dlg,&DtaPathDialog::accepted,this,&MainWindow::init);
        connect(dta_path_dlg,&DtaPathDialog::accepted,dta_path_dlg,&DtaPathDialog::deleteLater);
        connect(dta_path_dlg,&DtaPathDialog::rejected,this,&MainWindow::init);
        connect(dta_path_dlg,&DtaPathDialog::rejected,dta_path_dlg,&DtaPathDialog::deleteLater);
        dta_path_dlg->show();
        return;
    }

    // TESTING

//    MapItemModel* model = new MapItemModel(this);
////    MySortFilterProxyModel* proxy = new MySortFilterProxyModel(this);
////    proxy->setSourceModel(model);
////    proxy->setDynamicSortFilter(true);
////    proxy->sort(0,Qt::DescendingOrder);
//    ui->treeView->setModel(model);

//    connect(ui->pushButton,&QPushButton::clicked,ui->comboBox,&ComboBox::allowCurrent);
//    ui->comboBox->setDataIndex(model->findIndex(QVariantList() << Item::ColorsItemType));
//    ui->comboBox->proxyModel()->setFilterAllowed(false);
//    ui->comboBox->proxyModel()->setFilterInvalidKey(true);

//    connect(ui->pushButton_2,&QPushButton::clicked,ui->comboBox_2,&ComboBox::disallowCurrent);
//    ui->comboBox_2->setDataIndex(model->findIndex(QVariantList() << Item::ColorsItemType));
//    ui->comboBox_2->proxyModel()->setFilterAllowed(true);
//    ui->comboBox_2->proxyModel()->setFilterInvalidKey(true);

    init();

    // TESTING
    loadMap("C:\\Users\\rouve\\Documents\\Dawn of the Tiberium Age\\Maps\\Custom\\test2.map");
}

MainWindow::~MainWindow()
{
    delete m_OptionOpen;
    delete m_OptionPath;
    delete m_Parser;
    delete ui;
}

void MainWindow::loadMap(const QString &file_name)
{
    if (file_name.isEmpty()) return;

    QFileInfo fi(file_name);
    if (!fi.isReadable()) {
        qWarning() << "Unable to open file:" << file_name.toUtf8().data();
        return;
    }

    // check if already opened
    for (int x=0; x < ui->tabWidget->count(); x++) {
        Editor* editor = qobject_cast<Editor*>(ui->tabWidget->widget(x));
        if (!editor) continue;
        if (editor->mapFileInfo() == fi) {
            ui->tabWidget->setCurrentIndex(x);
            return;
        }
    }

    Editor* editor = new Editor(m_App,this);
    connect(editor,SIGNAL(updateRecentMaps()),this,SLOT(onUpdateRecentMaps()));
    bool ret = editor->readMap(fi);
    if (!ret) {
        qWarning() << "Failed to read map file:" << file_name.toUtf8().data();
        editor->deleteLater();
        return;
    }

    int index = ui->tabWidget->addTab(editor,fi.fileName());
    ui->tabWidget->setTabIcon(index,QIcon(QStringLiteral(":/icons/globe-48.png")));
    ui->tabWidget->setCurrentIndex(index);
}

void MainWindow::saveMap()
{
    Editor* editor = qobject_cast<Editor*>(ui->tabWidget->currentWidget());
    if (!editor) return;

    editor->writeMap();
}

void MainWindow::init()
{
    qInfo() << "DTA Folder:" << QDir::toNativeSeparators(m_App.dta()->baseDir().path()).toUtf8().data();
    if (!Dta::isBaseDirValid()) {
        QMessageBox msg_box;
        msg_box.setText(QStringLiteral(MSGBOX_DTA_PATH_FAILURE));
        msg_box.exec();
        exit(EXIT_FAILURE);
    }

    // quit button
    connect(ui->actionQuit,SIGNAL(triggered(bool)),qApp,SLOT(quit()));

    // map loading
    connect(ui->actionLoad_Map,SIGNAL(triggered(bool)),this,SLOT(onLoadMapClicked()));
    connect(ui->actionSave_Map,SIGNAL(triggered(bool)),this,SLOT(onExportMapClicked()));

    // update recent list
    updateRecentMaps();

    // update save button
    connect(ui->tabWidget,SIGNAL(currentChanged(int)),this,SLOT(onCurrentTabChanged(int)));
    onCurrentTabChanged(ui->tabWidget->currentIndex());

    // tabs
    connect(ui->tabWidget,SIGNAL(tabCloseRequested(int)),this,SLOT(onTabCloseRequested(int)));

    // about dialog
    m_AboutDialog = new AboutDialog();
    connect(ui->actionAbout,SIGNAL(triggered(bool)),m_AboutDialog,SLOT(show()));

    // ready msg
    qInfo() << "DTA Map Assistant initialized.";

    // open file given from cmd line
    QString open_file = m_Parser->value(*m_OptionOpen);
    if (!open_file.isEmpty()) {
        loadMap(open_file);
    }
}

void MainWindow::onLogReady(const QString &logstr, const QtMsgType &type, const QString &type_str, const QString &func_str, const QString &msg)
{
    Q_UNUSED(logstr);
    Q_UNUSED(func_str);

    QString color;
    bool bold = false;

    if (type == QtDebugMsg) {
        color = QStringLiteral("darkgrey");
        bold = false;
    } else if (type == QtInfoMsg) {
        color = QStringLiteral("black");
        bold = false;
    } else if (type == QtWarningMsg) {
        color = QStringLiteral("black");
        bold = true;
    } else if (type == QtCriticalMsg) {
        color = QStringLiteral("red");
        bold = false;
    } else if (type == QtFatalMsg) {
        color = QStringLiteral("red");
        bold = true;
    }

    QString data;

    if (!color.isEmpty()) {
        data.append("<font color=\"").append(color).append("\">");
    }
    if (bold) data.append(QStringLiteral("<b>"));

    data.append(type_str);
    data.append(' ');
    data.append(msg);

    if (bold) data.append("</b>");
    if (!color.isEmpty()) data.append("</font>");

    ui->editLog->append(data);
}

void MainWindow::onLoadMapClicked()
{
    //loadMap(QFileDialog::getOpenFileName(this,QStringLiteral("Load Map"), m_App.dta()->mapsDir().path(), QStringLiteral("DTA maps (*.map);;")));
}

void MainWindow::onExportMapClicked()
{
    saveMap();
}

void MainWindow::onCurrentTabChanged(const int &current_index)
{
    Q_UNUSED(current_index);
    Editor* editor = qobject_cast<Editor*>(ui->tabWidget->currentWidget());
    if (!editor) {
        ui->actionSave_Map->setEnabled(false);
        return;
    }

    ui->actionSave_Map->setEnabled(true);
}

void MainWindow::onTabCloseRequested(const int &index)
{
    Editor* editor = qobject_cast<Editor*>(ui->tabWidget->widget(index));
    if (editor) {
        ui->tabWidget->removeTab(index);
        editor->deleteLater();
    }
}

void MainWindow::onUpdateRecentMaps()
{
    ui->menuRecent->clear();
    QIcon icon(QStringLiteral(":/icons/globe-48.png"));
    foreach (QString file_name, m_App.recentMaps()) {
        QFileInfo fi(file_name);
        QAction* action = new QAction(icon,fi.fileName(),ui->menuRecent);
        action->setData(file_name);
        connect(action,SIGNAL(triggered(bool)),this,SLOT(onRecentMapClicked()));
        ui->menuRecent->addAction(action);
    }
}

void MainWindow::onRecentMapClicked()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (!action) return;
    loadMap(action->data().toString());
}

void MainWindow::onModelDestroyed()
{
    qDebug() << "MODEL DESTROYED";
}

//MySortFilterProxyModel::~MySortFilterProxyModel() {}

//bool MySortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
//{
//    QVariant leftData = sourceModel()->data(left);
//    QVariant rightData = sourceModel()->data(right);

//    qDebug() << leftData << rightData;

//    return QSortFilterProxyModel::lessThan(left,right);
//}
