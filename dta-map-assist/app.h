#ifndef APP_H
#define APP_H

#include <QSharedPointer>
#include <QStringList>

class Dta;

class App
{
public:
    explicit App();
    virtual ~App();

    Dta* dta() { return data()->dta; }

    QStringList recentMaps() { return data()->recentMaps; }
    void addRecentMap(const QString& filename);

private:
    struct Data
    {
        explicit Data();
        virtual ~Data();
        Dta* dta;
        QStringList recentMaps;
        void readRecentMaps();
        void writeRecentMaps();
    };
    typedef QSharedPointer<Data> DataPtr;

    DataPtr m_DataPtr;

    Data* data() { return m_DataPtr.data(); }
};

#endif // APP_H
