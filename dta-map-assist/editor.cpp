#include "editor.h"
#include "ui_editor.h"

#include "dta.h"
#include "mapfile.h"

// TEMP
#include "itemdelegate.h"

#include <QDebug>

#define HEX_ICON ":/icons/hexadecimal-48.png"

Editor::Editor(const App &app, QWidget *parent) : QWidget(parent), ui(new Ui::Editor), m_App(app)
{
    ui->setupUi(this);

    m_MapFile = new MapFile(this);
    connect(m_MapFile->model(),&MapItemModel::importStarted,this,&Editor::onImportStarted);
    connect(m_MapFile->model(),&MapItemModel::importFinished,this,&Editor::onImportFinished);

    ui->treeView->setItemDelegate(new ItemDelegate());
    ui->treeView->setModel(m_MapFile->model());

    // sides
    connect(ui->buttonAllowPlayerSide,&QPushButton::clicked,ui->comboBoxDisallowedPlayerSides,&ComboBox::allowCurrent);
    ui->comboBoxDisallowedPlayerSides->proxyModel()->setFilterAllowed(false);
    ui->comboBoxDisallowedPlayerSides->proxyModel()->setFilterInvalidKey(true);

    connect(ui->buttonDisallowPlayerSide,&QPushButton::clicked,ui->comboBoxAllowedPlayerSides,&ComboBox::disallowCurrent);
    ui->comboBoxAllowedPlayerSides->proxyModel()->setFilterAllowed(true);
    ui->comboBoxAllowedPlayerSides->proxyModel()->setFilterInvalidKey(true);

    //colors
    connect(ui->buttonAllowPlayerColor,&QPushButton::clicked,ui->comboBoxDisallowedPlayerColors,&ComboBox::allowCurrent);
    ui->comboBoxDisallowedPlayerColors->proxyModel()->setFilterAllowed(false);
    ui->comboBoxDisallowedPlayerColors->proxyModel()->setFilterInvalidKey(true);

    connect(ui->buttonDisallowPlayerColor,&QPushButton::clicked,ui->comboBoxAllowedPlayerColors,&ComboBox::disallowCurrent);
    ui->comboBoxAllowedPlayerColors->proxyModel()->setFilterAllowed(true);
    ui->comboBoxAllowedPlayerColors->proxyModel()->setFilterInvalidKey(true);

//    // Name
//    connect(ui->editName,SIGNAL(textChanged(QString)),this,SLOT(onNameChanged(QString)));

//    // Author
//    connect(ui->editAuthor,SIGNAL(textChanged(QString)),this,SLOT(onAuthorChanged(QString)));

//    // Coop Mission
//    connect(ui->checkIsCoopMission,SIGNAL(toggled(bool)),this,SLOT(onIsCoopToggled(bool)));

//    // Min Players
//    connect(ui->spinMinPlayers,SIGNAL(valueChanged(int)),this,SLOT(onMinPlayersValueChanged(int)));

//    // Max Players
//    connect(ui->spinMaxPlayers,SIGNAL(valueChanged(int)),this,SLOT(onMaxPlayersValueChanged(int)));

//    // Enforce Max Players
//    connect(ui->checkForceMaxPlayers,SIGNAL(toggled(bool)),this,SLOT(onEnforceMaxPlayersToggled(bool)));

//    // Hex View
//    ui->tableViewHexView->setModel(new QStandardItemModel());
}

Editor::~Editor()
{
    delete ui;
}

QFileInfo Editor::mapFileInfo() const
{
    return m_MapFile->fileInfo();
}

bool Editor::readMap(const QFileInfo &map_file_info)
{
    m_MapFile->read(map_file_info);

    // Basic
//    Basic* basic = m_MapFile->section<Basic>();
//    if (!basic) return false;

//    // general settings
//    basic->connectWidget(ui->editName,basic->option(Basic::Name));
//    basic->connectWidget(ui->editAuthor,basic->option(Basic::Author));
//    basic->connectWidget(ui->spinMinPlayers,basic->option(Basic::MinPlayer));
//    basic->connectWidget(ui->spinMaxPlayers,basic->option(Basic::MaxPlayer));
//    basic->connectWidget(ui->checkIsCoopMission,basic->option(Basic::IsCoopMission));

//    // disallowed player sides
//    ui->comboBoxAllowedPlayerSides->connectInverse(ui->comboBoxDisallowedPlayerSides);
//    ui->comboBoxAllowedPlayerSides->fill();
//    basic->connectWidget(ui->comboBoxDisallowedPlayerSides,basic->option(Basic::DisallowedPlayerSides));
//    connect(ui->buttonAllowPlayerSide,&QPushButton::clicked,ui->comboBoxDisallowedPlayerSides,&SideComboBox::removeCurrent,Qt::UniqueConnection);
//    connect(ui->buttonDisallowPlayerSide,&QPushButton::clicked,ui->comboBoxAllowedPlayerSides,&SideComboBox::removeCurrent,Qt::UniqueConnection);
//    connect(ui->enemyHouseGroupBox,&EnemyHouseGroupBox::usedSidesExport,ui->comboBoxDisallowedPlayerSides,&SideComboBox::setUsedIds);

//    // disallowed player colors
//    ui->comboBoxAllowedPlayerColors->connectInverse(ui->comboBoxDisallowedPlayerColors);
//    ui->comboBoxAllowedPlayerColors->fill();
//    basic->connectWidget(ui->comboBoxDisallowedPlayerColors,basic->option(Basic::DisallowedPlayerColors));
//    connect(ui->buttonAllowPlayerColor,&QPushButton::clicked,ui->comboBoxDisallowedPlayerColors,&ColorComboBox::removeCurrent,Qt::UniqueConnection);
//    connect(ui->buttonDisallowPlayerColor,&QPushButton::clicked,ui->comboBoxAllowedPlayerColors,&ColorComboBox::removeCurrent,Qt::UniqueConnection);
//    connect(ui->enemyHouseGroupBox,&EnemyHouseGroupBox::usedColorsExport,ui->comboBoxDisallowedPlayerColors,&ColorComboBox::setUsedIds);

//    // enemy houses
//    ui->enemyHouseGroupBox->addSectionOptions(basic);
//    ui->enemyHouseGroupBox->setWaypointsSection(m_MapFile->section<Waypoints>());
//    connect(ui->buttonDisallowedPlayerSidesImport,&QPushButton::clicked,ui->enemyHouseGroupBox,&EnemyHouseGroupBox::exportUsedSides);
//    connect(ui->buttonDisallowedPlayerColorsImport,&QPushButton::clicked,ui->enemyHouseGroupBox,&EnemyHouseGroupBox::exportUsedColors);

//    // Forced Options
//    ForcedOptions* forced_options = m_MapFile->section<ForcedOptions>();
//    if (!forced_options) return false;

//    foreach (TristateCheckBox* chk, ui->groupBoxForcedOptions->findChildren<TristateCheckBox*>(QString(),Qt::FindDirectChildrenOnly)) {
//        if (!chk) continue;
//        chk->setTristate(true);

//        Option* option = forced_options->option(chk->objectName().toUtf8());
//        if (!option) continue;

//        forced_options->connectWidget(chk,option);
//        option->emitCurrentValue();
//    }

//    foreach (AbstractEnumComboBox* cmb, ui->groupBoxForcedOptions->findChildren<AbstractEnumComboBox*>(QString(),Qt::FindDirectChildrenOnly)) {
//        if (!cmb) continue;
//        cmb->fill();

//        Option* option = forced_options->option(cmb->objectName().toUtf8());
//        if (!option) continue;

//        forced_options->connectWidget(cmb,option);
//        option->emitCurrentValue();
//    }

    // Read ini
//    bool read_ret = m_MapFile->read(map_file_info);
//    if (!read_ret) return false;

//    Option* opt_name2 = basic->option("Name");
//    if (opt_name2) qDebug() << opt_name2->value();

//    Option* opt_gm = basic->option(Basic::GameMode);
//    if (opt_gm) qDebug() << opt_gm->value();

//    Option* opt_official = basic->option(Basic::Official);
//    if (opt_official) qDebug() << opt_official->toTristate().value() << opt_official->toTristate().outputFormat();

//    Option* dps = basic->option(Basic::DisallowedPlayerSides);
//    if (dps) qDebug() << dps->toIntList();

    return true;

//    m_MapFile = map_file_info;

//    bool ret = m_Ini->readFile(m_MapFile);
//    if (!ret) {
//        return false;
//    }

//    m_App.addRecentMap(map_file_info.filePath());
//    emit(updateRecentMaps());

//    // read modifications from json file
//    QString json_file = map_file_info.path().append('/').append(map_file_info.completeBaseName()).append(QStringLiteral(".json"));
//    m_ModsFile.setFile(json_file);
//    m_Mods->readFile(m_ModsFile.filePath());

//    // load colors from rules.ini
//    bool color_ret = m_Color.readFile(m_App.dta()->rulesIniFile(),Color::Default);
//    if (!color_ret) {
//        qWarning() << "Unable to read default colors!";
//    }

//    color_ret = m_Color.readFile(m_App.dta()->gameOptionsIniFile(),Color::MP);
//    if (!color_ret) {
//        qWarning() << "Unable to read multiplayer colors!";
//    }

//    // forced options values from map
//    foreach (QObject* obj, ui->groupBoxForcedOptions->children()) {
//        if (!obj || !obj->isWidgetType()) continue;

//        QComboBox* cmb = qobject_cast<QComboBox*>(obj);
//        if (cmb) {
//            cmb->blockSignals(true);
//            int combo_value = m_Ini->forcedOptionComboBox(obj->objectName());
//            cmb->setCurrentIndex(cmb->findData(combo_value));
//            cmb->blockSignals(false);
//        }

//        QCheckBox* chk = qobject_cast<QCheckBox*>(obj);
//        if (chk) {
//            chk->blockSignals(true);
//            chk->setCheckState(static_cast<Qt::CheckState>(m_Ini->forcedOptionCheckBox(obj->objectName())));
//            chk->blockSignals(false);
//        }
//    }

//    // Name
//    ui->editName->blockSignals(true);
//    ui->editName->setText(m_Ini->stringValue<Ini::Basic>(Ini::Name));
//    ui->editName->blockSignals(false);

//    // Author
//    ui->editAuthor->blockSignals(true);
//    ui->editAuthor->setText(m_Ini->stringValue<Ini::Basic>(Ini::Author));
//    ui->editAuthor->blockSignals(false);

//    // Co-Op Mission
//    ui->checkIsCoopMission->blockSignals(true);
//    ui->checkIsCoopMission->setChecked(m_Ini->boolValue<Ini::Basic>(Ini::IsCoopMission));
//    ui->checkIsCoopMission->blockSignals(false);

//    // Min Players
//    ui->spinMinPlayers->blockSignals(true);
//    ui->spinMinPlayers->setValue(m_Ini->intValue<Ini::Basic>(Ini::MinPlayer));
//    ui->spinMinPlayers->blockSignals(false);

//    // Max Players
//    ui->spinMaxPlayers->blockSignals(true);
//    ui->spinMaxPlayers->setValue(m_Ini->intValue<Ini::Basic>(Ini::MaxPlayer));
//    ui->spinMaxPlayers->blockSignals(false);

//    // Enforce Max Players
//    ui->checkForceMaxPlayers->blockSignals(true);
//    ui->checkForceMaxPlayers->setChecked(m_Ini->boolValue<Ini::Basic>(Ini::EnforceMaxPlayers));
//    ui->checkForceMaxPlayers->blockSignals(false);

//    // colors
//    ui->coopEnemyHouses->setColor(m_Color);

//    // coop enemies
//    ui->coopEnemyHouses->setMaxPlayer(ui->spinMaxPlayers->value());
//    ui->coopEnemyHouses->setEnabled(ui->checkIsCoopMission->isChecked());
//    ui->coopEnemyHouses->setIni(m_Ini);
//    ui->coopEnemyHouses->importFromIni();

//    // dis-/allowed player sides
//    ui->comboBoxDisallowedPlayerSides->clear();
//    ui->comboBoxAllowedPlayerSides->reset();
//    bool import_enabled = ui->checkIsCoopMission->isChecked();
//    ui->buttonDisallowedPlayerSidesImport->setEnabled(import_enabled);
//    connect(ui->buttonDisallowedPlayerSidesImport, &QPushButton::clicked, this, &Editor::onDisallowedSidesImport, Qt::UniqueConnection);
//    connect(ui->buttonAllowPlayerSide, &QPushButton::clicked, this, &Editor::onAllowSideClicked, Qt::UniqueConnection);
//    connect(ui->buttonDisallowPlayerSide, &QPushButton::clicked, this, &Editor::onDisallowSideClicked, Qt::UniqueConnection);

//    foreach (QString id_str, m_Ini->stringValue<Ini::Basic>(Ini::DisallowedPlayerSides).split(',')) {
//        bool ok = false;
//        int id = id_str.toInt(&ok);
//        if (ok) ui->comboBoxDisallowedPlayerSides->addSideId(id);
//        if (ok) ui->comboBoxAllowedPlayerSides->removeSideId(id);
//    }

//    // dis-/allowed player colors
//    ui->comboBoxDisallowedPlayerColors->setColor(m_Color);
//    ui->comboBoxDisallowedPlayerColors->clear();
//    ui->comboBoxAllowedPlayerColors->setColor(m_Color);
//    ui->buttonDisallowedPlayerColorsImport->setEnabled(import_enabled);
//    connect(ui->buttonDisallowedPlayerColorsImport, &QPushButton::clicked, this, &Editor::onDisallowedColorsImport, Qt::UniqueConnection);
//    connect(ui->buttonAllowPlayerColor, &QPushButton::clicked, this, &Editor::onAllowColorClicked, Qt::UniqueConnection);
//    connect(ui->buttonDisallowPlayerColor, &QPushButton::clicked, this, &Editor::onDisallowColorClicked, Qt::UniqueConnection);

//    foreach (QString id_str, m_Ini->stringValue<Ini::Basic>(Ini::DisallowedPlayerColors).split(',')) {
//        bool ok = false;
//        int id = id_str.toInt(&ok);
//        if (ok) ui->comboBoxDisallowedPlayerColors->addColorId(id);
//        if (ok) ui->comboBoxAllowedPlayerColors->removeColorId(id);
//    }

//    // Hex View
//    ui->comboBoxHexView->clear();
//    QIcon hex_icon = QIcon(QStringLiteral(HEX_ICON));
//    QStringList hex_sections = m_Ini->hexSections();
//    foreach (QString hex_section, hex_sections) {
//        ui->comboBoxHexView->addItem(hex_icon,hex_section,hex_section);
//    }
//    connect(ui->comboBoxHexView, SIGNAL(currentIndexChanged(int)), this, SLOT(onHexViewIndexChanged(int)), Qt::UniqueConnection);
//    onHexViewIndexChanged(ui->comboBoxHexView->currentIndex());

//    return true;
}

bool Editor::writeMap()
{
//    if (!m_FileInfo.isReadable()) return false;

//    // write changes to original file
//    bool ret = m_Ini->writeFile(m_MapFile);
//    writeFileMessage(m_MapFile.filePath(),ret);

//    //qDebug() << writeFileName("TEST");

    return true;
}

void Editor::onImportStarted()
{

}

void Editor::onImportFinished(bool success)
{
    if (!success) {
        return;
    }

    // Sides
    ui->comboBoxAllowedPlayerSides->setDataIndex(m_MapFile->model()->findIndex(QVariantList() << Item::SidesItemType));
    ui->comboBoxDisallowedPlayerSides->setDataIndex(m_MapFile->model()->findIndex(QVariantList() << Item::SidesItemType));

    // Colors
    ui->comboBoxAllowedPlayerColors->setDataIndex(m_MapFile->model()->findIndex(QVariantList() << Item::ColorsItemType));
    ui->comboBoxDisallowedPlayerColors->setDataIndex(m_MapFile->model()->findIndex(QVariantList() << Item::ColorsItemType));

}

//QString Editor::enumDesc(QMetaEnum me, const int &value, const int &valid_min, const int &valid_max)
//{
//    if (value < valid_min || value > valid_max) return QStringLiteral("---");
//    QString name = me.valueToKey(value);
//    name.replace(me.name(),QStringLiteral(""));
//    return name;
//}

//QString Editor::writeFileName(const Mods::GameMode &game_mode)
//{
//    return writeFileName(QMetaEnum::fromType<Mods::GameMode>().valueToKey(game_mode));
//}

//QString Editor::writeFileName(const QString &game_mode_tag) const
//{
//    if (game_mode_tag.isEmpty()) return QString();
//    QString filename = m_MapFile.path();
//    filename.append('/');
//    filename.append(m_MapFile.completeBaseName());
//    filename.append('_');
//    filename.append(game_mode_tag);
//    filename.append('.');
//    filename.append(m_MapFile.suffix());
//    return filename;
//}

//void Editor::writeFileMessage(const QString &file_name, const bool &success)
//{
//    QString msg = (QStringLiteral("Writing file: %1 - %2."));
//    if (success) {
//        qInfo() << msg.arg(QDir::toNativeSeparators(file_name)).arg(QStringLiteral("success")).toUtf8().data();
//    } else {
//        msg = msg.arg(QDir::toNativeSeparators(file_name)).arg(QStringLiteral("failed"));
//        qWarning() << msg.toUtf8().data();

//        QMessageBox msg_box;
//        msg_box.setText(msg);
//        msg_box.exec();
//    }
//}

//void Editor::updateDisallowedSides()
//{
//    QStringList list;
//    foreach (int id, ui->comboBoxDisallowedPlayerSides->sideIds()) {
//        list.append(QString::number(id));
//    }
//    m_Ini->setStringValue<Ini::Basic>(Ini::DisallowedPlayerSides,list.join(','));
//}

//void Editor::updateDisallowedColors()
//{
//    QStringList list;
//    foreach(int id, ui->comboBoxDisallowedPlayerColors->colorIds()) {
//        list.append(QString::number(id));
//    }
//    m_Ini->setStringValue<Ini::Basic>(Ini::DisallowedPlayerColors,list.join(','));
//}

//void Editor::onForcedOptionsComboIndexChanged(const int &index)
//{
//    Q_UNUSED(index);

//    QComboBox* cmb = qobject_cast<QComboBox*>(sender());
//    if (!cmb) return;

//    m_Ini->setForcedOptionComboBox(cmb->objectName(),cmb->currentData().toInt());
//}

//void Editor::onForcedOptionsCheckStateChanged(const int &state)
//{
//    QCheckBox* chk = qobject_cast<QCheckBox*>(sender());
//    if (!chk) return;

//    m_Ini->setForcedOptionCheckBox(chk->objectName(),static_cast<Ini::ForceState>(state));
//}

//void Editor::onIsCoopToggled(const bool &checked)
//{
//    m_Ini->setBoolValue<Ini::Basic>(Ini::IsCoopMission,checked,true);
//    ui->coopEnemyHouses->setEnabled(checked);
//    ui->coopEnemyHouses->exportToIni();
//    ui->buttonDisallowedPlayerSidesImport->setEnabled(checked);
//    ui->buttonDisallowedPlayerColorsImport->setEnabled(checked);
//}

//void Editor::onMinPlayersValueChanged(const int &value)
//{
//    ui->spinMaxPlayers->setMinimum(value);
//    m_Ini->setIntValue<Ini::Basic>(Ini::MinPlayer,value);
//}

//void Editor::onMaxPlayersValueChanged(const int &value)
//{
//    ui->spinMinPlayers->setMaximum(value);
//    m_Ini->setIntValue<Ini::Basic>(Ini::MaxPlayer,value);
//    ui->coopEnemyHouses->setMaxPlayer(value);
//}

//void Editor::onDisallowedSidesImport()
//{
//    ui->comboBoxAllowedPlayerSides->reset();
//    ui->comboBoxDisallowedPlayerSides->clear();

//    foreach (int s, ui->coopEnemyHouses->usedSides()) {
//        ui->comboBoxAllowedPlayerSides->removeSideId(s);
//        ui->comboBoxDisallowedPlayerSides->addSideId(s);
//    }

//    updateDisallowedSides();
//}

//void Editor::onDisallowSideClicked()
//{
//    int index = ui->comboBoxAllowedPlayerSides->currentIndex();
//    if (index < 0) return;

//    int side_id = ui->comboBoxAllowedPlayerSides->currentData().toInt();
//    ui->comboBoxAllowedPlayerSides->removeSideId(side_id);
//    ui->comboBoxDisallowedPlayerSides->addSideId(side_id);

//    updateDisallowedSides();
//}

//void Editor::onAllowSideClicked()
//{
//    int index = ui->comboBoxDisallowedPlayerSides->currentIndex();
//    if (index < 0) return;

//    int side_id = ui->comboBoxDisallowedPlayerSides->currentData().toInt();
//    ui->comboBoxDisallowedPlayerSides->removeSideId(side_id);
//    ui->comboBoxAllowedPlayerSides->addSideId(side_id);

//    updateDisallowedSides();
//}

//void Editor::onDisallowedColorsImport()
//{
//    ui->comboBoxAllowedPlayerColors->reset();
//    ui->comboBoxDisallowedPlayerColors->clear();

//    foreach (int s, ui->coopEnemyHouses->usedColors()) {
//        ui->comboBoxAllowedPlayerColors->removeColorId(s);
//        ui->comboBoxDisallowedPlayerColors->addColorId(s);
//    }

//    updateDisallowedColors();
//}

//void Editor::onDisallowColorClicked()
//{
//    int index = ui->comboBoxAllowedPlayerColors->currentIndex();
//    if (index < 0) return;

//    int color_id = ui->comboBoxAllowedPlayerColors->currentData().toInt();
//    ui->comboBoxAllowedPlayerColors->removeColorId(color_id);
//    ui->comboBoxDisallowedPlayerColors->addColorId(color_id);

//    updateDisallowedColors();
//}

//void Editor::onAllowColorClicked()
//{
//    int index = ui->comboBoxDisallowedPlayerColors->currentIndex();
//    if (index < 0) return;

//    int color_id = ui->comboBoxDisallowedPlayerColors->currentData().toInt();
//    ui->comboBoxDisallowedPlayerColors->removeColorId(color_id);
//    ui->comboBoxAllowedPlayerColors->addColorId(color_id);

//    updateDisallowedColors();
//}

//void Editor::onHexViewIndexChanged(const int &index)
//{
//    Q_UNUSED(index);
//    QString section_name = ui->comboBoxHexView->currentData().toString();
//    if (section_name.isEmpty()) return;

//    QByteArray data = m_Ini->binarySectionData(section_name);
//    QStandardItemModel* model = qobject_cast<QStandardItemModel*>(ui->tableViewHexView->model());
//    if (!model) return;
//    model->clear();

//    int col_count = 0;
//    int row_count = 0;
//    QString row;
//    for (int x=0; x < data.count(); x++) {
//        QString num = QString::number((quint8)data.at(x),16);
//        while (num.length() < 2) {
//            num.prepend("0");
//        }

//        QStandardItem* item = new QStandardItem(num);
//        model->setItem(row_count,col_count,item);

//        if (col_count >= 7) {
//            col_count=0;
//            row_count++;
//        } else {
//            col_count++;
//        }
//    }

//    ui->tableViewHexView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

//    //    exit(EXIT_SUCCESS);
//}

