#include "app.h"

#include "dta.h"

#include <QSettings>

#define RECENT_MAPS_MAXLEN 10
#define RECENT_SETTINGS_KEY "RECENT_MAPS"

// Class App

App::App()
{
    m_DataPtr = DataPtr(new Data);
}

App::~App()
{

}

void App::addRecentMap(const QString &filename)
{
    if (data()->recentMaps.contains(filename)) {
        data()->recentMaps.removeAll(filename);
    }
    data()->recentMaps.prepend(filename);

    while (data()->recentMaps.count() > RECENT_MAPS_MAXLEN) {
        data()->recentMaps.removeLast();
    }

    data()->writeRecentMaps();
}

// Class App::Data

App::Data::Data()
{
    // dta settings
    //dta = new Dta();

    // recent maps
    readRecentMaps();
}

App::Data::~Data()
{
    // dta settings
    //delete dta;
}

void App::Data::readRecentMaps()
{
    QSettings settings;
    recentMaps = settings.value(QStringLiteral(RECENT_SETTINGS_KEY)).toStringList();
}

void App::Data::writeRecentMaps()
{
    QSettings settings;
    settings.setValue(QStringLiteral(RECENT_SETTINGS_KEY),recentMaps);
}
