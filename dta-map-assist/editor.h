#ifndef EDITOR_H
#define EDITOR_H

#include "app.h"
//#include "color.h"

#include <QWidget>
#include <QFileInfo>
#include <QMap>
#include <QMetaEnum>

class MapFile;
//class Tristate;

namespace Ui {
class Editor;
}

class Editor : public QWidget
{
    Q_OBJECT
public:
    explicit Editor(const App& app, QWidget *parent = Q_NULLPTR);
    virtual ~Editor();

    QFileInfo mapFileInfo() const;

//    static QString gameTypeDesc(const int& game_type) { return cmbValueDesc<GameType>(game_type,Classic,Enhanced); }
//    static QString techLevelDesc(const int& tl) { return cmbValueDesc<TechLevel>(tl,TechLevel7,TechLevel1); }
//    static QString creditsDesc(const int& credits) { return cmbValueDesc<Credits>(credits,Credits20000,Credits2500); }

//    template <typename T>
//    static QString cmbValueDesc(const int& value, const int& valid_min, const int& valid_max) { return enumDesc(QMetaEnum::fromType<T>(),value,valid_min,valid_max); }

signals:
    void updateRecentMaps();

public slots:
    bool readMap(const QFileInfo& map_file_info);
    bool writeMap();

private:
    Ui::Editor *ui;

    App m_App;

    MapFile* m_MapFile;

private slots:
    void onImportStarted();
    void onImportFinished(bool success);
};

#endif // EDITOR_H
