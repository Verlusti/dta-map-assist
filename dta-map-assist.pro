TEMPLATE = subdirs

SUBDIRS += \
    dta-map-library \
    dta-map-assist

# project locations
dta-map-library = dta-map-library
dta-map-assist = dta-map-assist

dta-map-assist.depends = dta-map-library
