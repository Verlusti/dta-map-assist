#ifndef TRISTATE_H
#define TRISTATE_H

#include "dta-map-library_global.h"

#include <QObject>
#include <QVariant>
#include <QDebug>

class DTAMAPLIBRARYSHARED_EXPORT Tristate
{
    Q_GADGET
public:
    enum Value
    {
        Unset = Qt::PartiallyChecked,
        False = Qt::Unchecked,
        True = Qt::Checked
    };
    Q_ENUM(Value)

    enum OutputFormat
    {
        TrueFalse,
        YesNo,
        OneZero
    };
    Q_ENUM(OutputFormat)

    Tristate() : m_Value(Unset), m_OutputFormat(TrueFalse), m_LockFormat(false) {}
    Tristate(const Tristate& other) : m_Value(other.m_Value), m_OutputFormat(other.m_OutputFormat), m_LockFormat(false) {}
    Tristate(const Value& value) : m_Value(value), m_OutputFormat(TrueFalse), m_LockFormat(false) {}
    Tristate(const bool& value) : m_Value(Unset), m_OutputFormat(TrueFalse), m_LockFormat(false) { *this = value; }
    Tristate(const Qt::CheckState& value) : m_Value(Unset), m_OutputFormat(TrueFalse), m_LockFormat(false) { *this = value; }
    Tristate(const int& value) : m_Value(Unset), m_OutputFormat(TrueFalse), m_LockFormat(false) { *this = value; }
    Tristate(const QByteArray& value) : m_Value(Unset), m_OutputFormat(TrueFalse), m_LockFormat(false) { *this = value; }
    Tristate(const QVariant& value) : m_Value(Unset), m_OutputFormat(TrueFalse), m_LockFormat(false) { *this = value.value<Tristate>(); }
    virtual ~Tristate();

    OutputFormat outputFormat() const { return m_OutputFormat; }
    void setOutputFormat(const OutputFormat& output_format) { m_OutputFormat = output_format; }
    bool trueFalse() const { return (m_OutputFormat == TrueFalse); }
    bool yesNo() const { return (m_OutputFormat == YesNo); }
    bool oneZero() const { return (m_OutputFormat == OneZero); }

    bool lockFormat() const { return m_LockFormat; }
    void setLockFormat(const bool& lock_format)  { m_LockFormat = lock_format; }

    Value value() const { return m_Value; }
    void setValue(const Value& value) { m_Value = value; }

    bool toBool() const { return (m_Value == True); }
    Qt::CheckState toCheckState() const { return static_cast<Qt::CheckState>(m_Value); }
    int toInt() const { return m_Value; }
    QByteArray toByteArray() const;
    QVariant toVariant() const { return QVariant::fromValue<Tristate>(*this); }

    Tristate& operator=(const Tristate& value) { m_Value = value.m_Value; if (!m_LockFormat) m_OutputFormat = value.m_OutputFormat; return *this; }
    Tristate& operator=(const Value& value) { m_Value = value; return *this; }
    Tristate& operator=(const bool& value) { m_Value = (value) ? True : False; return *this; }
    Tristate& operator=(const Qt::CheckState& value) { m_Value = static_cast<Value>(value); return *this; }
    Tristate& operator=(const int& value) { m_Value = static_cast<Value>(value); return *this; }
    Tristate& operator=(const QByteArray& value);
    Tristate& operator=(const QVariant& value) { m_Value = value.value<Tristate>().value(); return *this; }

    bool operator==(const Tristate& other) const { return (m_Value == other.m_Value); }
    bool operator!=(const Tristate& other) const { return !(m_Value == other.m_Value); }

    bool operator==(const Tristate::Value& other) const { return (m_Value == other); }
    bool operator!=(const Tristate::Value& other) const { return !(m_Value == other); }

    bool operator==(const bool& other) const { return (*this == Tristate(other)); }
    bool operator!=(const bool& other) const { return !(*this == Tristate(other)); }

    bool operator==(const Qt::CheckState& other) const { return (*this == Tristate(other)); }
    bool operator!=(const Qt::CheckState& other) const { return !(*this == Tristate(other)); }

    bool operator==(const int& other) const { return (m_Value == other); }
    bool operator!=(const int& other) const { return !(m_Value == other); }

    bool operator==(const QByteArray& other) const { return (*this == Tristate(other)); }
    bool operator!=(const QByteArray& other) const { return !(*this == Tristate(other)); }

    bool operator==(const QVariant& other) const { return (*this == other.value<Tristate>()); }
    bool operator!=(const QVariant& other) const { return !(*this == other.value<Tristate>()); }

    operator Value() const { return m_Value; }
    operator bool() const { return toBool(); }
    operator Qt::CheckState() const { return toCheckState(); }
    operator int() const { return m_Value; }
    operator QByteArray() const { return toByteArray(); }
    operator QVariant() const { return toVariant(); }

private:
    Value m_Value;
    OutputFormat m_OutputFormat;
    bool m_LockFormat;

    friend QDebug operator<<(QDebug debug, const Tristate &tristate);
};

inline bool operator==(const Tristate::Value& other, const Tristate& tristate) { return (tristate == other); }
inline bool operator!=(const Tristate::Value& other, const Tristate& tristate) { return (tristate != other); }

inline bool operator==(const bool& other, const Tristate& tristate) { return (tristate == other); }
inline bool operator!=(const bool& other, const Tristate& tristate) { return (tristate != other); }

inline bool operator==(const Qt::CheckState& other, const Tristate& tristate) { return (tristate == other); }
inline bool operator!=(const Qt::CheckState& other, const Tristate& tristate) { return (tristate != other); }

inline bool operator==(const int& other, const Tristate& tristate) { return (tristate == other); }
inline bool operator!=(const int& other, const Tristate& tristate) { return (tristate != other); }

inline bool operator==(const QByteArray& other, const Tristate& tristate) { return (tristate == other); }
inline bool operator!=(const QByteArray& other, const Tristate& tristate) { return (tristate != other); }

QDebug operator<<(QDebug debug, const Tristate &tristate);

#endif // TRISTATE_H
