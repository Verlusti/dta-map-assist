#include "ini.h"

#include <QDebug>

Ini::SectionData::SectionData()
{

}

Ini::SectionData::~SectionData()
{
    clear();
}

void Ini::SectionData::clear()
{
    m_Keys.clear();
    qDeleteAll(m_Values);
    m_Values.clear();
}

void Ini::SectionData::set(const QByteArray &key, const QByteArray &value)
{
    if (!m_Keys.contains(key)) m_Keys.append(key);
    QByteArray* value_ptr = m_Values.value(key,Q_NULLPTR);
    if (!value_ptr) {
        value_ptr = new QByteArray();
        m_Values.insert(key,value_ptr);
    }
    *value_ptr = value.trimmed();
}

void Ini::SectionData::unset(const QByteArray &key)
{
    m_Keys.removeAll(key);
    QByteArray* value_ptr = m_Values.value(key,Q_NULLPTR);
    m_Values.remove(key);
    delete value_ptr;
}

Ini::SectionData &Ini::SectionData::operator=(const Ini::SectionData &other)
{
    m_Keys = other.m_Keys;

    qDeleteAll(m_Values);
    m_Values.clear();

    foreach (QByteArray key, m_Values.keys()) {
        QByteArray* value = m_Values.value(key,Q_NULLPTR);
        if (!value) continue;
        QByteArray* value_copy = new QByteArray(*value);
        m_Values.insert(key,value_copy);
    }

    return *this;
}

// INI CLASS

Ini::Ini()
{

}

Ini::~Ini()
{
    clear();
}

void Ini::clear()
{
    m_SectionNames.clear();
    qDeleteAll(m_SectionData);
    m_SectionData.clear();
}

Ini::SectionData *Ini::add(const QByteArray &name)
{
    if (!m_SectionNames.contains(name)) m_SectionNames.append(name);
    SectionData* data = m_SectionData.value(name,Q_NULLPTR);
    if (!data) {
        data = new SectionData();
        m_SectionData.insert(name,data);
    }
    return data;
}

void Ini::remove(const QByteArray &name)
{
    m_SectionNames.removeAll(name);
    SectionData* data = m_SectionData.value(name,Q_NULLPTR);
    m_SectionData.remove(name);
    delete data;
}

Ini Ini::fromRawData(const QByteArray &ini_data)
{
    QByteArray data(ini_data);
    data.replace(QByteArrayLiteral("\r\n"),QByteArrayLiteral("\n"));
    QByteArrayList data_list = data.split('\n');

    SectionData* current_section = Q_NULLPTR;

    Ini ini;
    foreach (QByteArray row, data_list) {
        if (row.contains(';')) {
            row = row.left(row.indexOf(';')).trimmed();
        }
        if (row.isEmpty()) continue;

        // section
        if (row.startsWith('[') && row.endsWith(']') && row.length() > 2) {
            row.remove(row.length()-1,1);
            row.remove(0,1);
            current_section = new SectionData();
            ini.m_SectionNames.append(row);
            ini.m_SectionData.insert(row,current_section);
            continue;
        }

        // value
        if (!current_section) continue;
        if (!row.contains('=')) continue;

        int index = row.indexOf('=');
        QByteArray row_key = row.left(index);
        QByteArray row_value = row.right(row.length()-1-index);
        current_section->set(row_key,row_value);
    }
    return ini;
}

Ini Ini::fromFile(const QFileInfo &ini_file_info)
{
    QFile file(ini_file_info.filePath());
    if (!file.exists() || !file.open(QFile::ReadOnly)) return Ini();

    QByteArray ini_data = file.readAll();
    file.close();

    return Ini::fromRawData(ini_data);
}

Ini &Ini::operator=(const Ini& other)
{
    m_SectionNames = other.m_SectionNames;

    qDeleteAll(m_SectionData);
    m_SectionData.clear();

    foreach (QByteArray key, other.m_SectionData.keys()) {
        SectionData* section_data = other.m_SectionData.value(key,Q_NULLPTR);
        if (!section_data) continue;
        SectionData* section_data_copy = new SectionData(*section_data);
        m_SectionData.insert(key,section_data_copy);
    }

    return *this;
}


