#include "option.h"

#include <QDebug>

Option::Option(QObject *parent) : QObject(parent)
{
    m_Standard = false;
    m_ValueType = NoValue;
}

Option::Option(const Option::ValueType &value_type, QObject *parent) : QObject (parent)
{
    m_Standard = false;
    m_ValueType = value_type;
}

Option::~Option()
{

}

QByteArray Option::toByteArray(bool *ok) const
{
    if (m_ValueType != ByteArrayValue) {
        if (ok) *ok = false;
        return QByteArray();
    }
    if (ok) *ok = true;
    return m_Value.toByteArray();
}

QString Option::toString(bool *ok) const
{
    if (m_ValueType != ByteArrayValue) {
        if (ok) *ok = false;
        return QString();
    }
    if (ok) *ok = true;
    return m_Value.toString();
}

int Option::toInt(bool *ok) const
{
    if (m_ValueType != IntValue) {
        if (ok) *ok = false;
        return 0;
    }
    return m_Value.toInt(ok);
}

double Option::toDouble(bool *ok) const
{
    if (m_ValueType != DoubleValue) {
        if (ok) *ok = false;
        return 0;
    }
    return m_Value.toDouble(ok);
}

Tristate Option::toTristate(bool *ok) const
{
    if (m_ValueType != TristateValue) {
        if (ok) *ok = false;
        return Tristate();
    }
    if (ok) *ok = true;
    return m_TristateValue;
}

Qt::CheckState Option::toCheckState(bool *ok) const
{
    if (m_ValueType != TristateValue) {
        if (ok) *ok = false;
        return Tristate();
    }
    if (ok) *ok = true;
    return m_TristateValue;
}

IntList Option::toIntList(bool *ok) const
{
    if (m_ValueType != IntListValue) {
        if (ok) *ok = false;
        return IntList();
    }
    if (ok) *ok = true;
    return m_Value.value<IntList>();
}

void Option::clear()
{
    bool emit_ = (m_Value.isValid());
    m_Value = QVariant();
    m_TristateValue = Tristate::Unset;
    if (emit_) emit(cleared());
}

void Option::emitCurrentValue()
{
    if (m_ValueType == ByteArrayValue) {
        emit(byteArrayChanged(toByteArray()));
    } else if (m_ValueType == IntValue) {
        bool ok = false;
        int int_value = toInt(&ok);
        if (ok) emit(intChanged(int_value));
    } else if (m_ValueType == DoubleValue) {
        bool ok = false;
        double double_value = toDouble(&ok);
        if (ok) emit(doubleChanged(double_value));
    } else if (m_ValueType == TristateValue) {
        emit(tristateChanged(toTristate()));
    } else if (m_ValueType == IntListValue) {
        emit(intListChanged(toIntList()));
    } else if (m_ValueType == VariantListValue) {

    }
}

bool Option::isValid() const
{
    if (m_ValueType != TristateValue && !m_Value.isValid()) return false;
    return true;
}

bool Option::setByteArray(const QByteArray &value)
{
    if (m_ValueType != ByteArrayValue) return false;
    if (m_Value == value) return true;
    m_Value = value;
    emit(byteArrayChanged(value));
    emit(stringChanged(QString(value)));
    return true;
}

bool Option::setInt(const int &value)
{
    if (m_ValueType != IntValue) return false;
    if (m_Value == value) return true;
    m_Value = value;
    emit(intChanged(value));
    return true;
}

bool Option::setDouble(const double &value)
{
    if (m_ValueType != DoubleValue) return false;
    if (m_Value == value) return true;
    m_Value = value;
    emit(doubleChanged(value));
    return true;
}

bool Option::setTristate(const Tristate &value)
{
    if (m_ValueType != TristateValue) return false;
    if (m_TristateValue == value) return true;
    m_TristateValue = value;
    emit(tristateChanged(m_TristateValue));
    emit(checkStateChanged(m_TristateValue.toCheckState()));
    return true;
}

bool Option::setTristateOutputFormat(const Tristate::OutputFormat &output_format)
{
    if (m_ValueType != TristateValue) return false;
    m_TristateValue.setLockFormat(false);
    m_TristateValue.setOutputFormat(output_format);
    m_TristateValue.setLockFormat(true);
    return true;
}

bool Option::setIntList(const IntList &value)
{
    if (m_ValueType != IntListValue) return false;
    if (m_Value.value<IntList>() == value) return true;
    m_Value.setValue(value);
    emit(intListChanged(value));
    return true;
}

bool Option::valueImport(const QByteArray value)
{
    if (m_ValueType == ByteArrayValue) {
        return setByteArray(value);
    } else if (m_ValueType == IntValue) {
        bool ok = false;
        int int_value = value.toInt(&ok);
        if (!ok) return false;
        return setInt(int_value);
    } else if (m_ValueType == DoubleValue) {
        bool ok = false;
        double double_value = value.toDouble(&ok);
        if (!ok) return false;
        return setDouble(double_value);
    } else if (m_ValueType == TristateValue) {
        return setTristate(value);
    } else if (m_ValueType == IntListValue) {
        IntList list;
        foreach (QByteArray ba, value.split(',')) { list.append(ba.toInt()); }
        return setIntList(list);
    } else if (m_ValueType == VariantListValue) {

    }

    return false;
}

QByteArray Option::valueExport(bool *ok)
{
    if (!isValueTypeValid() || !isValid()) {
        if (ok) *ok = false;
        return QByteArray();
    }

    if (m_ValueType == ByteArrayValue) {
        return toByteArray(ok);
    } else if (m_ValueType == IntValue) {
        bool tmp_ok = false;
        int tmp = toInt(&tmp_ok);
        if (ok) *ok = tmp_ok;
        return (tmp_ok) ? QByteArray::number(tmp) : QByteArray();
    } else if (m_ValueType == DoubleValue) {
        bool tmp_ok = false;
        double tmp = toDouble(&tmp_ok);
        if (ok) *ok = tmp_ok;
        return (tmp_ok) ? QByteArray::number(tmp,'f',6) : QByteArray();
    } else if (m_ValueType == TristateValue) {
        return toTristate(ok);
    } else if (m_ValueType == IntListValue) {
        QList<int> list = toIntList(ok);
        QByteArrayList ba_list;
        foreach (int x, list) { ba_list.append(QByteArray::number(x)); }
        return ba_list.join(',');
    } else if (m_ValueType == VariantListValue) {

    }

    if (ok) *ok = false;
    return QByteArray();
}



