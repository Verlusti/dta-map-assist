#ifndef DISALLOWEDPLAYERCOLORSOPTION_H
#define DISALLOWEDPLAYERCOLORSOPTION_H

#include "basicoption.h"

class DTAMAPLIBRARYSHARED_EXPORT DisallowedPlayerColorsOption : public BasicOption
{
    Q_GADGET
public:
    DisallowedPlayerColorsOption() : BasicOption() {}
    DisallowedPlayerColorsOption(const QString & text) : BasicOption(text) {}
    DisallowedPlayerColorsOption(const QIcon & icon, const QString & text) : BasicOption(icon,text) {}
    DisallowedPlayerColorsOption(int rows, int columns = 1) : BasicOption(rows,columns) {}
    virtual ~DisallowedPlayerColorsOption();

    virtual int type() const { return DisallowedPlayerColorsOptionItemType; }

    virtual int itemSync() const { return DisallowedPlayerColorsItemSync; }
    virtual void sync(Item* source, const QVariant& value, int role);

protected:
    virtual void setup();
};

#endif // DISALLOWEDPLAYERCOLORSOPTION_H
