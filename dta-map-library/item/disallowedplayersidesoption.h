#ifndef DISALLOWEDPLAYERSIDESOPTION_H
#define DISALLOWEDPLAYERSIDESOPTION_H

#include "basicoption.h"

class DTAMAPLIBRARYSHARED_EXPORT DisallowedPlayerSidesOption : public BasicOption
{
    Q_GADGET
public:
    DisallowedPlayerSidesOption() : BasicOption() {}
    DisallowedPlayerSidesOption(const QString & text) : BasicOption(text) {}
    DisallowedPlayerSidesOption(const QIcon & icon, const QString & text) : BasicOption(icon,text) {}
    DisallowedPlayerSidesOption(int rows, int columns = 1) : BasicOption(rows,columns) {}
    virtual ~DisallowedPlayerSidesOption();

    virtual int type() const { return DisallowedPlayerSidesOptionItemType; }

    virtual int itemSync() const { return DisallowedPlayerSidesItemSync; }
    virtual void sync(Item* source, const QVariant& value, int role);

protected:
    virtual void setup();
};

#endif // DISALLOWEDPLAYERSIDESOPTION_H
