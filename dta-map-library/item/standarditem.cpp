#include "standarditem.h"

StandardItem::~StandardItem()
{

}

void StandardItem::setData(const QVariant &value, int role)
{
    Item::setData(value,role);
    if (role != KeyRole) return;

    setText(value.toString());
}

void StandardItem::setup()
{
    setText(staticMetaObject.className());
    setIcon(standardIcon());
}

