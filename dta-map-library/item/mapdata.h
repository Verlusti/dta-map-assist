#ifndef MAPDATA_H
#define MAPDATA_H

#include "item.h"

class MapData : public Item
{
    Q_GADGET
public:
    MapData() : Item() {}
    MapData(const QString & text) : Item(text) {}
    MapData(const QIcon & icon, const QString & text) : Item(icon,text) {}
    MapData(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~MapData();

    virtual int type() const { return MapDataItemType; }
    virtual bool hasValue() const { return false; }

protected:
    virtual void setup();
};

#endif // MAPDATA_H
