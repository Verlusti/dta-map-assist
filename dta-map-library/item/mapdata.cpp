#include "mapdata.h"

#include "basic.h"
#include "forcedoptions.h"

MapData::~MapData()
{

}

void MapData::setup()
{
    setData(type(),KeyRole);
    setText(staticMetaObject.className());
    setIcon(QIcon(QStringLiteral(":/icons/mapdata/mapdata.png")));

    addChild<Basic>();
    addChild<ForcedOptions>();
}
