#include "color.h"

#include "dta.h"
#include "ini.h"

#include "disallowedplayercolorsoption.h"

#include <QDebug>
#include <QStringList>
#include <QRgb>
#include <QMutex>
#include <QMutexLocker>
#include <QMetaEnum>
#include <QPainter>
#include <QDir>

#define COLOR_SECTION "Colors"
#define MP_COLOR_SECTION "MPColors"

#define COLOR_ICON_INVALID ":/icons/color/color_-1.png"

static Dta::UpdateFuntion<&Color::updateValues> reg;

Color::~Color()
{

}

void Color::setData(const QVariant &value, int role)
{
    if (role != KeyRole) {
        Item::setData(value,role);
        return;
    }

    bool ok = false;
    int id = value.toInt(&ok);
    if (!ok) id = InvalidColor;

    Item::setData(id,KeyRole);
    setIcon(idToIcon(id));
    setText(idToDesc(id));
}

void Color::sync(Item *source, const QVariant &value, int role)
{
    if (role != ValueRole) return;

    DisallowedPlayerColorsOption* opt = dynamic_cast<DisallowedPlayerColorsOption*>(source);
    if (!opt) return;

    if (data(KeyRole) <= InvalidColor) return;
    bool allowed = !value.toByteArray().split(',').contains(data(KeyRole).toByteArray());
    setData(allowed,AllowedRole);
}

int Color::min(bool include_invalid)
{
     if (include_invalid) return InvalidColor;
     QMutexLocker lock(mapMutex());
     QMap<int, QByteArray>& id_to_name = idToName();
     if (id_to_name.isEmpty()) return InvalidColor;
     return id_to_name.keys().first();
}

int Color::max()
{
    QMutexLocker lock(mapMutex());
    QMap<int, QByteArray>& id_to_name = idToName();
    if (id_to_name.isEmpty()) return InvalidColor;
    return id_to_name.keys().last();
}

QList<int> Color::colorsAvailable(bool include_invalid)
{
    if (include_invalid) {
        QList<int> list = colorsAvailable(false);
        list.prepend(InvalidColor);
        return list;
    }
    QMutexLocker lock(mapMutex());
    return idToName().keys();
}

bool Color::isColorValid(const int &id)
{
    QMutexLocker lock(mapMutex());
    return idToName().contains(id);
}

QByteArray Color::idToName(const int &id)
{
    QMutexLocker lock(mapMutex());
    if (id <= InvalidColor) return QMetaEnum::fromType<Id>().valueToKey(InvalidColor);
    return idToName().value(id);
}

int Color::nameToId(const QByteArray &name)
{
    QMutexLocker lock(mapMutex());
    return nameToId().value(name,InvalidColor);
}

QColor Color::idToColor(const int &id)
{
    QMutexLocker lock(mapMutex());
    return idToColor().value(id);
}

Color::ColorType Color::idToType(const int &id)
{
    QMutexLocker lock(mapMutex());
    return idToType().value(id,InvalidType);
}

QPixmap Color::idToPixmap(const int &id)
{
    if (!isColorValid(id)) {
        return QPixmap(COLOR_ICON_INVALID);
    }

    bool with_border = withBorder();
    QPixmap pixmap(32,32);
    if (!with_border) {
        pixmap.fill(idToColor(id));
    } else {
        pixmap.fill(Qt::black);
        QPainter *paint = new QPainter(&pixmap);
        paint->fillRect(4,4,24,24,idToColor(id));
        delete paint;
    }
    return pixmap;
}

QByteArray Color::typeString(const int &type)
{
    return QMetaEnum::fromType<ColorType>().valueToKey(type);
}

QString Color::idToDesc(const int &id)
{
    QString color_name = idToName((id)).data();
    QString type_str = idToTypeString(id);
    if (color_name.isEmpty()) color_name = QStringLiteral("_Color_");
    QString desc_ = QStringLiteral("[%1] %2 (%3)");
    if (id == InvalidColor) {
        desc_ = desc_.arg(QStringLiteral("x")).arg(color_name).arg(type_str);
    } else {
        desc_ = desc_.arg(id).arg(color_name).arg(type_str);
    }
    return desc_;
}

void Color::updateValues()
{
    QMutexLocker lock(mapMutex());

    // delete current data
    idToName().clear();
    nameToId().clear();
    idToColor().clear();
    idToType().clear();

    // read default colors    
    qInfo() << "Reading colors from:" << QDir::toNativeSeparators(Dta::file(Dta::RulesIni).filePath()).toUtf8().data();
    int count = parseSection(Ini::fromFile(Dta::file(Dta::RulesIni)),QByteArrayLiteral(COLOR_SECTION),Default);
    qInfo() << "Colors: Found default colors:" << count;

    // read multiplayer colors
    qInfo() << "Reading colors from:" << QDir::toNativeSeparators(Dta::file(Dta::GameOptionsIni).filePath()).toUtf8().data();
    count = parseSection(Ini::fromFile(Dta::file(Dta::GameOptionsIni)),QByteArrayLiteral(MP_COLOR_SECTION),MultiPlayer);
    qInfo() << "Colors: Found multi-player colors:" << count;
}

bool Color::withBorder()
{
    QMutexLocker lock(borderMutex());
    return withBorderPrivate();
}

void Color::setWithBorder(const bool &with_border)
{
    QMutexLocker lock(borderMutex());
    withBorderPrivate() = with_border;
}

void Color::setup()
{
    setData(InvalidColor,KeyRole);
}

QMutex *Color::mapMutex()
{
    static QMutex mutex;
    return &mutex;
}

QMap<int, QByteArray> &Color::idToName()
{
    static QMap<int, QByteArray> map;
    return map;
}

QMap<QByteArray, int> &Color::nameToId()
{
    static QMap<QByteArray, int> map;
    return map;
}

QMap<int, QColor> &Color::idToColor()
{
    static QMap<int, QColor> map;
    return map;
}

QMap<int, Color::ColorType> &Color::idToType()
{
    static QMap<int, Color::ColorType> map;
    return map;
}

QMutex *Color::borderMutex()
{
    static QMutex mutex;
    return &mutex;
}

bool &Color::withBorderPrivate()
{
    static bool with_border = true;
    return with_border;
}

int Color::parseSection(const Ini &ini, const QByteArray &section_name, const ColorType &type)
{
    Ini::SectionData* section_data = ini.data(section_name);
    if (!section_data) return 0;

    QMap<int, QByteArray>& id_to_name = idToName();
    QMap<QByteArray, int>& name_to_id = nameToId();
    QMap<int, QColor>& id_to_color = idToColor();
    QMap<int, ColorType>& id_to_type = idToType();

    int count = 0;
    int id = 1;
    foreach (QByteArray key, section_data->keys()) {
        QByteArray name = key.trimmed();
        QByteArrayList list = section_data->value(key).trimmed().split(',');

        bool valid = false;
        if (type == Default && list.count() == 3) valid = true;
        if (type == MultiPlayer && list.count() == 4) valid = true;
        if (!valid) continue;

        qreal h_r = list.value(0).toDouble() / 255;
        qreal s_g = list.value(1).toDouble() / 255;
        qreal v_b = list.value(2).toDouble() / 255;

        int new_id = (type == Default) ? id : list.value(3).toInt();
        QColor color = (type == Default) ? QColor::fromHsvF(h_r,s_g,v_b).toRgb() : QColor::fromRgbF(h_r,s_g,v_b);

        id_to_name.insert(new_id,name);
        name_to_id.insert(name,new_id);
        id_to_color.insert(new_id,color);
        id_to_type.insert(new_id,type);

        id++;
        id++;
        count++;
    }
    return count;
}
