#ifndef COLOR_H
#define COLOR_H

#include "item.h"

class Ini;

class QMutex;

class DTAMAPLIBRARYSHARED_EXPORT Color : public Item
{
    Q_GADGET
public:
    enum ColorType
    {
        InvalidType = -1,
        Default,
        MultiPlayer
    };
    Q_ENUM(ColorType)

    enum Id
    {
        InvalidColor = -1
    };
    Q_ENUM(Id)

    Color() : Item() {}
    Color(const QString & text) : Item(text) {}
    Color(const QIcon & icon, const QString & text) : Item(icon,text) {}
    Color(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~Color();

    virtual int type() const { return ColorItemType; }
    virtual bool hasValue() const { return false; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

    virtual int itemSync() const { return DisallowedPlayerColorsItemSync; }
    virtual void sync(Item* source, const QVariant& value, int role);

    static int min(bool include_invalid = false);
    static int max();
    static QList<int> colorsAvailable(bool include_invalid = false);
    static bool isColorValid(const int& id);

    static QByteArray idToName(const int& id);
    static int nameToId(const QByteArray& name);
    static QColor idToColor(const int& id);
    static ColorType idToType(const int& id);
    static QByteArray idToTypeString(const int& id) { return typeString(idToType(id)); }
    static QPixmap idToPixmap(const int& id);
    static QIcon idToIcon(const int& id) { return QIcon(idToPixmap(id)); }
    static QByteArray typeString(const int& type);
    static QString idToDesc(const int& id);

    static void updateValues();

    static bool withBorder();
    static void setWithBorder(const bool& with_border);

protected:
    virtual void setup();

private:
    int m_Id;

    void init();

    static QMutex* mapMutex();
    static QMap<int, QByteArray>& idToName();
    static QMap<QByteArray, int>& nameToId();
    static QMap<int, QColor>& idToColor();
    static QMap<int, ColorType>& idToType();

    static QMutex* borderMutex();
    static bool& withBorderPrivate();

    static int parseSection(const Ini& ini, const QByteArray& section_name, const ColorType& type);
};

#endif // COLOR_H
