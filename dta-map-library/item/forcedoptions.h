#ifndef FORCEDOPTIONS_H
#define FORCEDOPTIONS_H

#include "sectionitem.h"

class DTAMAPLIBRARYSHARED_EXPORT ForcedOptions : public SectionItem
{
    Q_GADGET
public:    
    ForcedOptions() : SectionItem() {}
    ForcedOptions(const QString & text) : SectionItem(text) {}
    ForcedOptions(const QIcon & icon, const QString & text) : SectionItem(icon,text) {}
    ForcedOptions(int rows, int columns = 1) : SectionItem(rows,columns) {}
    virtual ~ForcedOptions();

    virtual int type() const { return ForcedOptionsItemType; }

protected:
    virtual void setup();
};

#endif // FORCEDOPTIONS_H
