#ifndef SIDES_H
#define SIDES_H

#include "item.h"

class DTAMAPLIBRARYSHARED_EXPORT Sides : public Item
{
    Q_GADGET
public:
    Sides() : Item() {}
    Sides(const QString & text) : Item(text) {}
    Sides(const QIcon & icon, const QString & text) : Item(icon,text) {}
    Sides(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~Sides();

    virtual int type() const { return SidesItemType; }
    virtual bool hasValue() const { return false; }

protected:
    virtual void setup();
};

#endif // SIDES_H
