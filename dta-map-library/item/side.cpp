#include "side.h"

#include "disallowedplayersidesoption.h"

#include <QMetaEnum>
#include <QDebug>

Side::~Side()
{

}

void Side::setData(const QVariant &value, int role)
{
    if (role != KeyRole) {
        Item::setData(value,role);
        return;
    }

    bool ok = false;
    int id = value.toInt(&ok);
    if (!ok) id = InvalidSide;

    Item::setData(id,KeyRole);
    setIcon(idToIcon(id));
    setText(idToDesc(id));
}

void Side::sync(Item *source, const QVariant &value, int role)
{
    if (role != ValueRole) return;

    DisallowedPlayerSidesOption* opt = dynamic_cast<DisallowedPlayerSidesOption*>(source);
    if (!opt) return;

    if (data(KeyRole) <= InvalidSide) return;
    bool allowed = !value.toByteArray().split(',').contains(data(KeyRole).toByteArray());
    setData(allowed,AllowedRole);
}

QList<int> Side::sidesAvailable(bool include_invalid)
{
    QList<int> list;
    for (int v = min(include_invalid); v <= Soviet; v++) {
        list.append(static_cast<Id>(v));
    }
    return list;
}

QByteArray Side::idToName(const int &id)
{
    if (id <= InvalidSide) return QMetaEnum::fromType<Id>().valueToKey(InvalidSide);
    return QMetaEnum::fromType<Id>().valueToKey(id);
}

Side::Id Side::nameToId(const QByteArray &name)
{
    QMetaEnum me = QMetaEnum::fromType<Id>();
    for(int x=0; x<me.keyCount(); x++) {
        QByteArray key = me.key(x);
        if (key.toLower() == name.toLower().trimmed()) {
            return static_cast<Id>(me.value(x));
        }
    }
    return InvalidSide;
}

QPixmap Side::idToPixmap(const int &id)
{
    int id_ = isSideValid(id) ? id : -1;
    QString file = QStringLiteral(":/icons/side/side_%1.png");
    return QPixmap(file.arg(id_));
}

QString Side::idToDesc(const int &id)
{
    QString color_name = idToName((id)).data();
    if (color_name.isEmpty()) color_name = QStringLiteral("_Side_");
    QString desc_ = QStringLiteral("[%1] %2");
    if (id == InvalidSide) {
        desc_ = desc_.arg(QStringLiteral("x")).arg(color_name);
    } else {
        desc_ = desc_.arg(id).arg(color_name);
    }
    return desc_;
}

void Side::setup()
{
    setData(InvalidSide,KeyRole);
}

