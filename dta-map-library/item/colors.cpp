#include "colors.h"

#include "color.h"

Colors::~Colors()
{

}

void Colors::setup()
{
    setData(type(),KeyRole);
    setText(staticMetaObject.className());
    setIcon(QIcon(QStringLiteral(":/icons/color/colors.png")));

    QList<int> list = Color::colorsAvailable(true);
    foreach (int id, list) {
        Color* item = addChild<Color>();
        item->setData(id,KeyRole);
        item->setData(true,AllowedRole);
        if (id == Color::InvalidColor) item->setData(true,InvalidKeyRole);
    }
}

