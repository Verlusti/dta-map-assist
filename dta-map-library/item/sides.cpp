#include "sides.h"

#include "side.h"

Sides::~Sides()
{

}

void Sides::setup()
{
    setData(type(),KeyRole);
    setText(staticMetaObject.className());
    setIcon(QIcon(QStringLiteral(":/icons/side/sides.png")));

    QList<int> list = Side::sidesAvailable(true);
    foreach (int id, list) {
        Side* item = addChild<Side>();
        item->setData(id,KeyRole);
        item->setData(true,AllowedRole);
        if (id == Side::InvalidSide) item->setData(true,InvalidKeyRole);
    }
}
