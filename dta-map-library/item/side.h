#ifndef SIDE_H
#define SIDE_H

#include "item.h"

#include <QObject>
#include <QPixmap>
#include <QIcon>

class DTAMAPLIBRARYSHARED_EXPORT Side : public Item
{
    Q_GADGET
public:
    enum Id
    {
        InvalidSide = -1,
        GDI = 0,
        Nod = 1,
        Allies = 2,
        Soviet = 3
    };
    Q_ENUM(Id)

    Side() : Item() {}
    Side(const QString & text) : Item(text) {}
    Side(const QIcon & icon, const QString & text) : Item(icon,text) {}
    Side(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~Side();

    virtual int type() const { return SideItemType; }
    virtual bool hasValue() const { return false; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

    virtual int itemSync() const { return DisallowedPlayerSidesItemSync; }
    virtual void sync(Item* source, const QVariant& value, int role);

    static int min(bool include_invalid = false) { return (include_invalid) ? InvalidSide : GDI; }
    static int max() { return Soviet; }
    static QList<int> sidesAvailable(bool include_invalid = false);
    static bool isSideValid(const int& id) { return (id >= min(false) && id <= max()); }

    static QByteArray idToName(const int& id);
    static Id nameToId(const QByteArray& name);
    static QPixmap idToPixmap(const int& id);
    static QIcon idToIcon(const int& id) { return QIcon(idToPixmap(id)); }
    static QString idToDesc(const int& id);

protected:
    virtual void setup();
};

#endif // SIDE_H
