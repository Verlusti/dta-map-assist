#include "forcedoptions.h"

#include "forcedoption.h"

#include <QDebug>
#include <QMetaEnum>

ForcedOptions::~ForcedOptions()
{

}

void ForcedOptions::setup()
{
    setData(type(),KeyRole);
    setData(staticMetaObject.className(),ReservedKeyRole);
    setText(staticMetaObject.className());
    setIcon(QIcon(QStringLiteral(":/icons/forcedoption/forcedoptions.png")));

    QMetaEnum me = QMetaEnum::fromType<ForcedOption::Options>();
    for (int x=0; x < me.keyCount(); x++) {
        ForcedOption* option = addChild<ForcedOption>();
        option->setData(me.value(x),KeyRole);
    }
}

