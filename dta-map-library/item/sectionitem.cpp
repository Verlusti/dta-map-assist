#include "sectionitem.h"

#include "item/standarditem.h"

#include <QDebug>

SectionItem::~SectionItem()
{

}

void SectionItem::setData(const QVariant &value, int role)
{
    Item::setData(value,role);
    if (role != KeyRole) return;

    setText(value.toString());
}

void SectionItem::import(const QByteArray &key, const QByteArray &value)
{
    Item* child_item = childWithReservedKey(key);
    if (child_item) {
        //qDebug() << "import:" << value;
        child_item->setData(value,ValueRole);
    } else {
        child_item = addChild<StandardItem>();
        child_item->setData(key,KeyRole);
        child_item->setData(value,ValueRole);
    }
}

void SectionItem::setup()
{
    setText(staticMetaObject.className());
    setIcon(QIcon(QStringLiteral(":/icons/section/section.png")));
}

