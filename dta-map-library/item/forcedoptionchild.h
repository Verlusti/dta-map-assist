#ifndef FORCEDOPTIONCHILD_H
#define FORCEDOPTIONCHILD_H

#include "item.h"

#include <QMetaEnum>
#include <QDebug>

class DTAMAPLIBRARYSHARED_EXPORT ForcedOptionChild : public Item
{
    Q_GADGET
public:
    ForcedOptionChild() : Item() {}
    ForcedOptionChild(const QString & text) : Item(text) {}
    ForcedOptionChild(const QIcon & icon, const QString & text) : Item(icon,text) {}
    ForcedOptionChild(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~ForcedOptionChild();

    virtual int type() const { return ForcedOptionChildItemType; }
    virtual bool hasValue() const { return false; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

protected:
    virtual QMetaEnum metaEnum() const = 0;

    virtual QPixmap pixmap() const;

};

template <typename EnumT>
class ForcedOptionChildT : public ForcedOptionChild
{
public:
    ForcedOptionChildT() : ForcedOptionChild() {}
    ForcedOptionChildT(const QString & text) : ForcedOptionChild(text) {}
    ForcedOptionChildT(const QIcon & icon, const QString & text) : ForcedOptionChild(icon,text) {}
    ForcedOptionChildT(int rows, int columns = 1) : ForcedOptionChild(rows,columns) {}
    virtual ~ForcedOptionChildT() {}

    static void addChildsToParent(Item* parent_item)
    {
        if (!parent_item) return;
        QMetaEnum me = QMetaEnum::fromType<EnumT>();
        for (int x=0; x < me.keyCount(); x++) {
            ForcedOptionChildT* child = parent_item->addChild<ForcedOptionChildT>();
            child->setData(me.value(x),KeyRole);
        }
    }

protected:
    virtual void setup()
    {
        setData(-1,KeyRole);
        setText(metaEnum().name());
    }

    virtual QMetaEnum metaEnum() const { return QMetaEnum::fromType<EnumT>(); }
};

#endif // FORCEDOPTIONCHILD_H
