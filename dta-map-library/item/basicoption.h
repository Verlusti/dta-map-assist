#ifndef BASICOPTION_H
#define BASICOPTION_H

#include "item.h"

class DTAMAPLIBRARYSHARED_EXPORT BasicOption : public Item
{
    Q_GADGET
public:
    enum Options {
        Name,
        Author,
        Percent,
        GameMode,
        HomeCell,
        InitTime,
        Official,
        EndOfGame,
        FreeRadar,
        MaxPlayer,
        MinPlayer,
        SkipScore,
        TrainCrate,
        TruckCrate,
        AltHomeCell,
        EnemyHouse0,
        EnemyHouse1,
        EnemyHouse2,
        EnemyHouse3,
        EnemyHouse4,
        EnemyHouse5,
        EnemyHouse6,
        OneTimeOnly,
        CarryOverCap,
        NewINIFormat,
        NextScenario,
        IsCoopMission,
        SkipMapSelect,
        CarryOverMoney,
        AltNextScenario,
        MultiplayerOnly,
        IceGrowthEnabled,
        EnforceMaxPlayers,
        VeinGrowthEnabled,
        DisallowedPlayerSides,
        TiberiumGrowthEnabled,
        DisallowedPlayerColors,
        IgnoreGlobalAITriggers,
        TiberiumDeathToVisceroid
    };
    Q_ENUM(Options)

    BasicOption() : Item() {}
    BasicOption(const QString & text) : Item(text) {}
    BasicOption(const QIcon & icon, const QString & text) : Item(icon,text) {}
    BasicOption(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~BasicOption();

    virtual int type() const { return BasicOptionItemType; }
    virtual bool hasValue() const { return true; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

protected:
    virtual void setup();

    virtual QPixmap pixmap() const;

    virtual void syncProtected(Item* source, const QVariant& value, int role, int invalid_id);
};

#endif // BASICOPTION_H
