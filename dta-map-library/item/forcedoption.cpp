#include "forcedoption.h"

#include "forcedoptionchild.h"

#include <QDebug>

ForcedOption::~ForcedOption()
{

}

void ForcedOption::setData(const QVariant &value, int role)
{
    if (role != KeyRole) {
        Item::setData(value,role);
        return;
    }

    bool ok = false;
    int id = value.toInt(&ok);
    if (!ok) id = -1;

    Item::setData(id,KeyRole);
    QByteArray name = QMetaEnum::fromType<Options>().valueToKey(id);
    if (!name.isEmpty()) Item::setData(name,ReservedKeyRole);
    if (name.isEmpty()) name = staticMetaObject.className();
    setText(name);
    setIcon(icon());

    if (id >= cmbClassicEnhanced && id <= cmbCredits && rowCount() == 0) {
        if (id == cmbClassicEnhanced) ForcedOptionChildT<ForcedOption::ClassicEnhanced>::addChildsToParent(this);
        else if (id == cmbTechLevel) ForcedOptionChildT<ForcedOption::TechLevel>::addChildsToParent(this);
        else if (id == cmbCredits) ForcedOptionChildT<ForcedOption::Credits>::addChildsToParent(this);
    }
}

void ForcedOption::setup()
{
    setData(-1,KeyRole);
    setText(staticMetaObject.className());
    setEditable(true);
}

QPixmap ForcedOption::pixmap() const
{
    bool ok = false;
    int id = data(KeyRole).toInt(&ok);
    if (!ok) id = -1;

    if (id < cmbClassicEnhanced) {
        return QPixmap(QStringLiteral(":/icons/forcedoption/invalid.png"));
    } else if (id >= cmbClassicEnhanced && id <= cmbCredits) {
        return QPixmap(QStringLiteral(":/icons/forcedoption/combobox.png"));
    } else if (id > cmbCredits) {
        return QPixmap(QStringLiteral(":/icons/forcedoption/checkbox.png"));
    }
    return QPixmap();
}
