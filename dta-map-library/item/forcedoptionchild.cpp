#include "forcedoptionchild.h"

#include "forcedoption.h"

#include <QDebug>

ForcedOptionChild::~ForcedOptionChild()
{

}

void ForcedOptionChild::setData(const QVariant &value, int role)
{
    if (role != KeyRole) {
        Item::setData(value,role);
        return;
    }

    bool ok = false;
    int id = value.toInt(&ok);
    if (!ok) id = -1;

    Item::setData(id,KeyRole);
    QString name = (id != -1) ? QString(metaEnum().valueToKey(id)) : QStringLiteral("---");
    if (name.isEmpty()) name = metaEnum().name();
//    name.prepend(QString(QStringLiteral("[%1] ")).arg(id));

    setText(name);
    setIcon(icon());
}

QPixmap ForcedOptionChild::pixmap() const
{
    QString name = metaEnum().name();
    if (name.isEmpty()) return QPixmap();

    bool ok = false;
    int id = data(KeyRole).toInt(&ok);
    if (!ok) id = -1;

    QString file_name = QStringLiteral(":/icons/%1/%1_%2.png");
    return QPixmap(file_name.arg(name.toLower()).arg(id));
}




