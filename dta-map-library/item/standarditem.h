#ifndef STANDARDITEM_H
#define STANDARDITEM_H

#include "item.h"

class DTAMAPLIBRARYSHARED_EXPORT StandardItem : public Item
{
    Q_GADGET
public:
    StandardItem() : Item() {}
    StandardItem(const QString & text) : Item(text) {}
    StandardItem(const QIcon & icon, const QString & text) : Item(icon,text) {}
    StandardItem(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~StandardItem();

    virtual int type() const { return StandardItemType; }
    virtual bool hasValue() const { return true; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

    static QPixmap standardPixmap() { return QPixmap(QStringLiteral(":/icons/standarditem/standarditem.png")); }
    static QIcon standardIcon() { return QIcon(standardPixmap()); }

protected:
    virtual void setup();
};

#endif // STANDARDITEM_H
