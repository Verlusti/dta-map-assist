#include "basicoption.h"

#include "standarditem.h"

#include <QMetaEnum>
#include <QDebug>

BasicOption::~BasicOption()
{

}

void BasicOption::setData(const QVariant &value, int role)
{
    setDataEnumOptions(value,role,QMetaEnum::fromType<Options>(),staticMetaObject.className());
}

void BasicOption::setup()
{
    setData(-1,KeyRole);
    setText(staticMetaObject.className());
    setEditable(true);
}

QPixmap BasicOption::pixmap() const
{
    bool ok = false;
    int id = data(KeyRole).toInt(&ok);
    if (!ok) id = -1;

    QByteArray name = QMetaEnum::fromType<Options>().valueToKey(id);
    if (name.isEmpty()) return StandardItem::standardPixmap();

    QPixmap pix(QString(QStringLiteral(":/icons/basic/%1.png")).arg(name.toLower().data()));
    if (pix.isNull()) pix = StandardItem::standardPixmap();
    return pix;
}

void BasicOption::syncProtected(Item *source, const QVariant &value, int role, int invalid_id)
{
    if (!source) return;
    if (role != AllowedRole) return;
    if (!value.isValid()) return;

    bool ok1 = false;
    int id = source->data(KeyRole).toInt(&ok1);
    if (!ok1) return;

    QByteArrayList list = data(ValueRole).toByteArray().split(',');
    QList<int> int_list;
    foreach(QByteArray ba, list) {
        bool ok2 = false;
        int i = ba.toInt(&ok2);
        if (!ok2 || i <= invalid_id) continue;

        if (!int_list.contains(i)) {
            int_list.append(i);
        }
    }

    if (value.toBool() == true) {
        int_list.removeOne(id);
    } else {
        if (!int_list.contains(id)) int_list.append(id);
    }

    std::sort(int_list.begin(),int_list.end());

    list.clear();
    foreach(int x, int_list) {
        list.append(QByteArray::number(x));
    }
    setData(list.join(','),ValueRole);
}

