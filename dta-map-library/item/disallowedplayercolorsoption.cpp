#include "disallowedplayercolorsoption.h"

#include "color.h"

DisallowedPlayerColorsOption::~DisallowedPlayerColorsOption()
{

}

void DisallowedPlayerColorsOption::sync(Item *source, const QVariant &value, int role)
{
    Color* color = dynamic_cast<Color*>(source);
    if (!color) return;
    syncProtected(source,value,role,Color::InvalidColor);
}

void DisallowedPlayerColorsOption::setup()
{
    setData(BasicOption::DisallowedPlayerColors,KeyRole);
    setText(staticMetaObject.className());
    setEditable(true);
}
