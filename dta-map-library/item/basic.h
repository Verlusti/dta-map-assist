#ifndef BASIC_H
#define BASIC_H

#include "sectionitem.h"

class DTAMAPLIBRARYSHARED_EXPORT Basic : public SectionItem
{
    Q_GADGET
public:
    Basic() : SectionItem() {}
    Basic(const QString & text) : SectionItem(text) {}
    Basic(const QIcon & icon, const QString & text) : SectionItem(icon,text) {}
    Basic(int rows, int columns = 1) : SectionItem(rows,columns) {}
    virtual ~Basic();

    virtual int type() const { return BasicItemType; }

protected:
    virtual void setup();
};

#endif // BASIC_H
