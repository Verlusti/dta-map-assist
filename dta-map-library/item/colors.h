#ifndef COLORGROUP_H
#define COLORGROUP_H

#include "item.h"

class DTAMAPLIBRARYSHARED_EXPORT Colors : public Item
{
    Q_GADGET
public:
    Colors() : Item() {}
    Colors(const QString & text) : Item(text) {}
    Colors(const QIcon & icon, const QString & text) : Item(icon,text) {}
    Colors(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~Colors();

    virtual int type() const { return ColorsItemType; }
    virtual bool hasValue() const { return false; }

protected:
    virtual void setup();
};

#endif // COLORGROUPITEM_H
