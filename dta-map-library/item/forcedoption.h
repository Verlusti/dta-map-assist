#ifndef FORCEDOPTION_H
#define FORCEDOPTION_H

#include "item.h"

class DTAMAPLIBRARYSHARED_EXPORT ForcedOption : public Item
{
    Q_GADGET
public:
    enum Options {
        cmbClassicEnhanced,
        cmbTechLevel,
        cmbCredits,
        chkIngameAllying,
        chkShortGame,
        chkRedeplMCV,
        chkCratesSkirm,
        chkCratesMP,
        chkNoBaddyCrates,
        chkMultiEngSkirm,
        chkMultiEngMP,
        chkStartingUnits,
        chkBuildOffAlly,
        chkQueuing,
        chkCrushableInfantry,
        chkVisceroids,
        chkScrapDebris,
        chkTiberium2Ore,
        chkExtremeAI,
        chkTurtlingAI,
        chkInfiniteTiberium,
        chkInstantUnload,
        chkSilos,
        chkDestrBridges,
        chkRevealShroud,
        chkGrowingShroud,
        chkStorms,
        chkTurboVehicles,
        chkNaval,
        chkSuperWeapons
    };
    Q_ENUM(Options)

    enum ClassicEnhanced
    {
        ClassicEnhancedUndefined = -1,
        Classic = 0,
        Enhanced = 1
    };
    Q_ENUM(ClassicEnhanced)

    enum TechLevel
    {
        TechLevelUndefined = -1,
        TechLevel7 = 0,
        TechLevel6 = 1,
        TechLevel5 = 2,
        TechLevel4 = 3,
        TechLevel3 = 4,
        TechLevel2 = 5,
        TechLevel1 = 6
    };
    Q_ENUM(TechLevel)

    enum Credits
    {
        CreditsUndefined = -1,
        Credits20000 = 0,
        Credits15000 = 1,
        Credits12500 = 2,
        Credits10000 = 3,
        Credits7500 = 4,
        Credits5000 = 5,
        Credits2500 = 6
    };
    Q_ENUM(Credits)

    ForcedOption() : Item() {}
    ForcedOption(const QString & text) : Item(text) {}
    ForcedOption(const QIcon & icon, const QString & text) : Item(icon,text) {}
    ForcedOption(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~ForcedOption();

    virtual int type() const { return ForcedOptionItemType; }
    virtual bool hasValue() const { return true; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

protected:
    virtual void setup();

    virtual QPixmap pixmap() const;
};

#endif // FORCEDOPTION_H
