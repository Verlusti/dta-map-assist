#ifndef SECTIONITEM_H
#define SECTIONITEM_H

#include "item.h"

class DTAMAPLIBRARYSHARED_EXPORT SectionItem : public Item
{
    Q_GADGET
public:
    SectionItem() : Item() {}
    SectionItem(const QString & text) : Item(text) {}
    SectionItem(const QIcon & icon, const QString & text) : Item(icon,text) {}
    SectionItem(int rows, int columns = 1) : Item(rows,columns) {}
    virtual ~SectionItem();

    virtual int type() const { return SectionItemType; }
    virtual bool hasValue() const { return false; }

    virtual void setData(const QVariant & value, int role = Qt::UserRole + 1);

    virtual void import(const QByteArray& key, const QByteArray& value);

protected:
    virtual void setup();
};

#endif // SECTIONITEM_H
