#include "basic.h"

#include "disallowedplayersidesoption.h"
#include "disallowedplayercolorsoption.h"

#include <QDebug>
#include <QMetaEnum>

Basic::~Basic()
{

}

void Basic::setup()
{
    setData(type(),KeyRole);
    setData(staticMetaObject.className(),ReservedKeyRole);
    setText(staticMetaObject.className());
    setIcon(QIcon(QStringLiteral(":/icons/basic/basic.png")));

    QMetaEnum me = QMetaEnum::fromType<BasicOption::Options>();
    for (int x=0; x < me.keyCount(); x++) {
        int enum_value = me.value(x);
        BasicOption* option = Q_NULLPTR;
        if (enum_value == BasicOption::DisallowedPlayerSides) {
            option = addChild<DisallowedPlayerSidesOption>();
            continue;
        } else if (enum_value == BasicOption::DisallowedPlayerColors) {
            option = addChild<DisallowedPlayerColorsOption>();
            continue;
        } else {
            option = addChild<BasicOption>();
        }
        option->setData(me.value(x),KeyRole);
    }
}
