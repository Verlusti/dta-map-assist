#include "disallowedplayersidesoption.h"

#include "side.h"

#include <algorithm>

DisallowedPlayerSidesOption::~DisallowedPlayerSidesOption()
{

}

void DisallowedPlayerSidesOption::sync(Item *source, const QVariant &value, int role)
{
    Side* side = dynamic_cast<Side*>(source);
    if (!side) return;
    syncProtected(source,value,role,Side::InvalidSide);
}

void DisallowedPlayerSidesOption::setup()
{
    setData(BasicOption::DisallowedPlayerSides,KeyRole);
    setText(staticMetaObject.className());
    setEditable(true);
}
