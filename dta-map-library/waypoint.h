#ifndef WAYPOINT_H
#define WAYPOINT_H

#include "position.h"

#include <QObject>
#include <QPixmap>
#include <QIcon>

class DTAMAPLIBRARYSHARED_EXPORT Waypoint
{
    Q_GADGET
public:
    enum Id
    {
        Invalid = -1,
        Player1 = 0,
        Player2 = 1,
        Player3 = 2,
        Player4 = 3,
        Player5 = 4,
        Player6 = 5,
        Player7 = 6,
        Player8 = 7,
        DefaultHomeCell = 98,
        DefaultAltHomeCell = 99
    };
    Q_ENUM(Id)

    Waypoint();
    virtual ~Waypoint();

    int id() const { return m_Id; }
    void setId(const int& id) { m_Id = id; }

    Position position() const { return m_Position; }
    void setPosition(const Position& position) { m_Position = position; }
    bool setPosition(const int& position);

    int x() const { return m_Position.x(); }

    int y() const { return m_Position.y(); }

    static QByteArray idToName(const int& id);
    //static Id nameToId(const QByteArray& name);
    static QPixmap idToPixmap(const int& id);
    static QIcon idToIcon(const int& id) { return QIcon(idToPixmap(id)); }

private:
    int m_Id;
    Position m_Position;
};

#endif // WAYPOINT_H
