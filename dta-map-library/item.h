#ifndef ITEM_H
#define ITEM_H

#include "dta-map-library_global.h"

#include <QStandardItem>

class QMetaEnum;

class DTAMAPLIBRARYSHARED_EXPORT Item : public QStandardItem
{
    Q_GADGET
public:
    enum DataRole
    {
        DefaultRole = Qt::UserRole + 1,
        KeyRole,
        ReservedKeyRole,
        InvalidKeyRole,
        ValueRole,
        AllowedRole
    };
    Q_ENUM(DataRole)

    enum ItemType
    {
        StandardItemType = UserType,
        SectionItemType,
        ColorsItemType,
        ColorItemType,
        SidesItemType,
        SideItemType,
        MapDataItemType,
        BasicItemType,
        BasicOptionItemType,
        DisallowedPlayerSidesOptionItemType,
        DisallowedPlayerColorsOptionItemType,
        ForcedOptionsItemType,
        ForcedOptionItemType,
        ForcedOptionChildItemType
    };
    Q_ENUM(ItemType)

    enum ItemSync
    {
        NoItemSync = -1,
        DisallowedPlayerSidesItemSync = 0,
        DisallowedPlayerColorsItemSync
    };
    Q_ENUM(ItemSync)

    Item() : QStandardItem(), m_Init(false), m_SyncBlocked(false) { setEditable(false); }
    Item(const QString & text) : QStandardItem(text), m_Init(false), m_SyncBlocked(false) { setEditable(false); }
    Item(const QIcon & icon, const QString & text) : QStandardItem(icon,text), m_Init(false), m_SyncBlocked(false) { setEditable(false); }
    Item(int rows, int columns = 1) : QStandardItem(rows,columns), m_Init(false), m_SyncBlocked(false) { setEditable(false); }
    virtual ~Item();

    void init();

    virtual int type() const = 0; // override
    virtual bool hasValue() const = 0; // override
    virtual QString displayText() const; // override if necessary

    virtual QVariant data(int role = Qt::UserRole + 1) const; // override if necessary
    virtual void setData(const QVariant &value, int role = Qt::UserRole + 1); // override if necessary

    virtual int itemSync() const { return NoItemSync; } // override if necessary
    virtual void sync(Item* source, const QVariant& value, int role) { Q_UNUSED(source); Q_UNUSED(value); Q_UNUSED(role); } // override if necessary
    bool syncBlocked() const { return m_SyncBlocked; }
    void setSyncBlocked(const bool& blocked) { m_SyncBlocked = blocked; }

    virtual void import(const QByteArray& key, const QByteArray& value) { Q_UNUSED(key); Q_UNUSED(value); } // override in parameter items

    void addChild(Item* child_item);
    template <typename ChildT> ChildT* addChild() { ChildT* child_item = new ChildT(); addChild(child_item); return child_item; }

    QVariantList keyPath() const;
    Item* childWithReservedKey(const QVariant& reserved_key) const;

protected:
    virtual void setup() = 0; // override

    virtual QPixmap pixmap() const { return QPixmap(); }
    QIcon icon() const { return QIcon(pixmap()); }

    virtual void setDataEnumOptions(const QVariant & value, int role, const QMetaEnum& meta_enum, const QByteArray& class_name);

private:
    bool m_Init;
    bool m_SyncBlocked;
};

#endif // ITEM_H
