#ifndef ITEMDELEGATE_H
#define ITEMDELEGATE_H

#include "dta-map-library_global.h"

#include <QStyledItemDelegate>

class Item;

class DTAMAPLIBRARYSHARED_EXPORT ItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    ItemDelegate(QWidget *parent = Q_NULLPTR);
    virtual ~ItemDelegate() override;

    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;

private slots:
    virtual void commitAndCloseEditor();

private:
    Item* itemFromIndex(const QModelIndex& index);
};

#endif // ITEMDELEGATE_H
