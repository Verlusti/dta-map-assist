#include "tristate.h"

#include <QDebug>

#define STATE_UNSET ""
#define STATE_TRUE "true"
#define STATE_FALSE "false"
#define STATE_YES "yes"
#define STATE_NO "no"
#define STATE_ONE "1"
#define STATE_ZERO "0"

Tristate::~Tristate()
{

}

QByteArray Tristate::toByteArray() const
{    
    if (m_Value != True && m_Value != False) {
        return QByteArrayLiteral(STATE_UNSET);
    }

    if (m_OutputFormat == TrueFalse) {
        return (m_Value == True) ? QByteArrayLiteral(STATE_TRUE) : QByteArrayLiteral(STATE_FALSE);
    } else if (m_OutputFormat == YesNo) {
        return (m_Value == True) ? QByteArrayLiteral(STATE_YES) : QByteArrayLiteral(STATE_NO);
    } else if (m_OutputFormat == OneZero) {
        return (m_Value == True) ? QByteArrayLiteral(STATE_ONE) : QByteArrayLiteral(STATE_ZERO);
    }

    return QByteArrayLiteral(STATE_UNSET);
}

Tristate &Tristate::operator=(const QByteArray &value)
{
    QByteArray lower = value.toLower().trimmed();
    if (lower == QByteArrayLiteral(STATE_TRUE)) {
        m_Value = True;
        if (!m_LockFormat) m_OutputFormat = TrueFalse;
    } else if (lower == QByteArrayLiteral(STATE_FALSE)) {
        m_Value = False;
        if (!m_LockFormat) m_OutputFormat = TrueFalse;
    } else if (lower == QByteArrayLiteral(STATE_YES)) {
        m_Value = True;
        if (!m_LockFormat) m_OutputFormat = YesNo;
    } else if (lower == QByteArrayLiteral(STATE_NO)) {
        m_Value = False;
        if (!m_LockFormat) m_OutputFormat = YesNo;
    } else if (lower == QByteArrayLiteral(STATE_ONE)) {
        m_Value = True;
        if (!m_LockFormat) m_OutputFormat = OneZero;
    } else if (lower == QByteArrayLiteral(STATE_ZERO)) {
        m_Value = False;
        if (!m_LockFormat) m_OutputFormat = OneZero;
    } else {
        m_Value = Unset;
    }
    return *this;
}

QDebug operator<<(QDebug debug, const Tristate &tristate)
{
    QDebugStateSaver saver(debug);
    debug.nospace() << tristate.m_Value;
    return debug;
}
