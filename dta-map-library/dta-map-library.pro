#-------------------------------------------------
#
# Project created by QtCreator 2018-10-28T14:21:31
#
#-------------------------------------------------

QT       -= gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dta-map-library
TEMPLATE = lib

DEFINES += DTAMAPLIBRARY_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    mapfile.cpp \
    section.cpp \
    log.cpp \
    option.cpp \
    tristate.cpp \
    option/enemyhouseoption.cpp \
    dta.cpp \
    widget/colorcombobox.cpp \
    widget/sidecombobox.cpp \
    widget/abstractidcombobox.cpp \
    widget/enemyhousegroupbox.cpp \
    widget/tristatecheckbox.cpp \
    widget/abstractenumcombobox.cpp \
    widget/classicenhancedcombobox.cpp \
    widget/techlevelcombobox.cpp \
    widget/creditscombobox.cpp \
    section/waypoints.cpp \
    position.cpp \
    widget/waypointcombobox.cpp \
    option/waypointoption.cpp \
    waypoint.cpp \
    mapitemmodel.cpp \
    item.cpp \
    item/colors.cpp \
    item/color.cpp \
    item/sides.cpp \
    item/side.cpp \
    widget/combobox.cpp \
    sortfilterproxymodel.cpp \
    item/forcedoptions.cpp \
    item/mapdata.cpp \
    item/forcedoption.cpp \
    item/forcedoptionchild.cpp \
    ini.cpp \
    itemdelegate.cpp \
    item/standarditem.cpp \
    item/sectionitem.cpp \
    item/basic.cpp \
    item/basicoption.cpp \
    item/disallowedplayersidesoption.cpp \
    item/disallowedplayercolorsoption.cpp

HEADERS += \
        dta-map-library_global.h \ 
    mapfile.h \
    section.h \
    log.h \
    option.h \
    tristate.h \
    option/enemyhouseoption.h \
    dta.h \
    widget/colorcombobox.h \
    widget/sidecombobox.h \
    widget/abstractidcombobox.h \
    widget/enemyhousegroupbox.h \
    widget/tristatecheckbox.h \
    widget/abstractenumcombobox.h \
    widget/classicenhancedcombobox.h \
    widget/techlevelcombobox.h \
    widget/creditscombobox.h \
    section/waypoints.h \
    position.h \
    widget/waypointcombobox.h \
    option/waypointoption.h \
    waypoint.h \
    mapitemmodel.h \
    item.h \
    item/colors.h \
    item/color.h \
    item/sides.h \
    item/side.h \
    widget/combobox.h \
    sortfilterproxymodel.h \
    item/forcedoptions.h \
    item/mapdata.h \
    item/forcedoption.h \
    item/forcedoptionchild.h \
    ini.h \
    itemdelegate.h \
    item/standarditem.h \
    item/sectionitem.h \
    item/basic.h \
    item/basicoption.h \
    item/disallowedplayersidesoption.h \
    item/disallowedplayercolorsoption.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    resources.qrc
