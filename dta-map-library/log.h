#ifndef LOG_H
#define LOG_H

#include "dta-map-library_global.h"

#include <QObject>
#include <QString>
#include <QByteArray>

class QMutex;

class Log;

class DTAMAPLIBRARYSHARED_EXPORT Logger : public QObject
{
    Q_OBJECT
public:
    explicit Logger() {}
    virtual ~Logger() {}
signals:
    void logReady(const QString& logstr, const QtMsgType& type, const QString& type_str, const QString& func_str, const QString& msg);
private:
    friend class Log;
};

class DTAMAPLIBRARYSHARED_EXPORT Log
{
    Q_GADGET
public:
    enum System {
        NoneSystem = 0,
        ConsoleSystem = 1 << 0,
        SyslogSystem = 1 << 1,
        InternalSystem = 1 << 2
    };
    Q_DECLARE_FLAGS(Systems, System)
    Q_FLAGS(Systems)

    explicit Log();
    virtual ~Log();

    static void installMessageHandler(bool enable = true);

    // syslog support?
    static bool syslogSupport();

    // log method selection
    static Systems systems();
    static void setSystems(const Log::Systems& systems);
    static void setSystem(const System& system, bool status);
    static void addSystem(const System& system) { setSystem(system,true); }
    static void removeSystem(const System& system) { setSystem(system,false); }

    // dynamic message handler
    static void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    // logger instance
    static Logger* logger();

private:
    // log method selection
    static Systems& systemsRef();
    static QMutex* systemsMutex();

    // console
    static void consoleMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg);

    // syslog
    static void syslogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    // internal
    static void internalMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Log::Systems)

#endif // LOG_H
