#include "sortfilterproxymodel.h"

#include "item.h"

#include <QDebug>

SortFilterProxyModel::SortFilterProxyModel(QObject* parent) : QSortFilterProxyModel (parent)
{
    m_FilterInvalidKey = false;
}

SortFilterProxyModel::~SortFilterProxyModel()
{

}

void SortFilterProxyModel::setFilterInvalidKey(const bool &filter_invalid_key)
{
    if (m_FilterInvalidKey == filter_invalid_key) return;
    m_FilterInvalidKey = filter_invalid_key;
    invalidateFilter();
}

void SortFilterProxyModel::setFilterAllowed(const bool &allowed)
{
    m_FilterAllowed = allowed;
    invalidateFilter();
}

void SortFilterProxyModel::unsetFilterAllowed()
{
    m_FilterAllowed.clear();
    invalidateFilter();
}

bool SortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (m_SourceRootIndex.isValid() && sourceParent != m_SourceRootIndex) return true;

    QModelIndex child = sourceParent.child(sourceRow,0);

    if (m_FilterInvalidKey) {
        if (child.data(Item::InvalidKeyRole) == true) return false;
    }

    if (m_FilterAllowed.isValid()) {
        if (child.data(Item::AllowedRole) != m_FilterAllowed) return false;
    }

    return true;
}
