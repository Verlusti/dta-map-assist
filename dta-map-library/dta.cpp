#include "dta.h"

#include <QDebug>
#include <QMutex>
#include <QMutexLocker>
#include <QSettings>
#include <QStringList>
#include <QMetaEnum>

// Registry
#define DTA_REGISTRY_PATH "HKEY_CURRENT_USER\\Software\\DawnOfTheTiberiumAge"
#define DTA_REGISTRY_ENTRY "InstallPath"

// Settings
#define DTA_SETTINGS_GROUP "DTA"
#define DTA_SETTINGS_BASEDIR "BaseDir"

// Directories
#define DTA_DIR_INI "INI"
#define DTA_DIR_RESOURCES "Resources"
#define DTA_DIR_MAPS "Maps"
//#define DTA_DIR_MAPS_COOP "Co-Op"
//#define DTA_DIR_MAPS_CUSTOM "Custom"

// Files
#define DTA_FILE_DTA_EXE "DTA.exe"
#define DTA_FILE_RULES_INI "Rules.ini"
#define DTA_FILE_GAMEOPTIONS_INI "GameOptions.ini"

// Status strings
#define DTA_STATUS_OK "OK"
#define DTA_STATUS_EMPTY "Empty folder name given."
#define DTA_STATUS_FOLDER_BAD "Folder does not exist or is not readable."
#define DTA_STATUS_EXE_MISSING "Folder does not contain DTA.exe."

Dta::Dta(QObject *parent) : QObject(parent)
{

}

Dta::~Dta()
{

}

Dta *Dta::instance()
{
    static Dta dta;
    return &dta;
}

QString Dta::baseDirFromRegistry()
{
    QSettings settings(QStringLiteral(DTA_REGISTRY_PATH),QSettings::NativeFormat);
    return settings.value(QStringLiteral(DTA_REGISTRY_ENTRY)).toString();
}

QDir Dta::directory(const Dta::Directory &directory_)
{
    QByteArray d;
    if (directory_ == Base) {
        // do nothing
    } else if (directory_ == Ini) {
        d = QByteArrayLiteral(DTA_DIR_INI);
    } else if (directory_ == Resources) {
        d = QByteArrayLiteral(DTA_DIR_RESOURCES);
    } else if (directory_ == Maps) {
        d = QByteArrayLiteral(DTA_DIR_MAPS);
    }

    QDir base_dir = baseDir();
    if (d.isEmpty()) return base_dir;
    return QDir(base_dir.filePath(d));
}

QFileInfo Dta::file(const Dta::File &file_)
{
    Directory d = Base;
    QByteArray f;
    if (file_ == DtaExe) {
        f = QByteArrayLiteral(DTA_FILE_DTA_EXE);
    } else if (file_ == RulesIni) {
        d = Ini;
        f = QByteArrayLiteral(DTA_FILE_RULES_INI);
    } else if (file_ == GameOptionsIni) {
        d = Resources;
        f = QByteArrayLiteral(DTA_FILE_GAMEOPTIONS_INI);
    }

    if (f.isEmpty()) return QFileInfo();
    QDir dir = directory(d);
    return dir.filePath(f);
}

void Dta::readSettings()
{
    QSettings settings;
    settings.beginGroup(QStringLiteral(DTA_SETTINGS_GROUP));
    QString base_dir = settings.value(QStringLiteral(DTA_SETTINGS_BASEDIR)).toString();
    setBaseDir(base_dir);
    settings.endGroup();
}

void Dta::writeSettings()
{
    QSettings settings;
    settings.beginGroup(QStringLiteral(DTA_SETTINGS_GROUP));
    settings.setValue(QStringLiteral(DTA_SETTINGS_BASEDIR),baseDir().path());
    settings.endGroup();
}

QDir Dta::baseDir()
{
    QMutexLocker lock(baseDirMutex());
    return QDir(baseDirPrivate());
}

void Dta::setBaseDir(const QDir &base_dir, bool no_update)
{
    baseDirMutex()->lock();
    baseDirPrivate() = base_dir;
    baseDirMutex()->unlock();

    if (!no_update) updateAll();
    instance()->baseDirChanged(QDir::toNativeSeparators(baseDir().path()));
}

Dta::Status Dta::baseDirStatus(const QDir &base_dir)
{
    if (base_dir.isRelative()) return Empty;
    if (!base_dir.isReadable()) return FolderBad;

    QFileInfo exe_info(base_dir.filePath(QStringLiteral(DTA_FILE_DTA_EXE)));
    if (!exe_info.isReadable()) return ExeMissing;
    return Ok;
}

QString Dta::statusString(const Dta::Status &status)
{
    if (status == Ok) return QStringLiteral(DTA_STATUS_OK);
    if (status == Empty) return QStringLiteral(DTA_STATUS_EMPTY);
    if (status == FolderBad) return QStringLiteral(DTA_STATUS_FOLDER_BAD);
    if (status == ExeMissing) return QStringLiteral(DTA_STATUS_EXE_MISSING);
    return QString();
}

//QList<QPair<QByteArray, QByteArray> > Dta::readSectionFromIni(const QFileInfo &file_info, const QByteArray &section)
//{
//    QList<QPair<QByteArray, QByteArray> > pair_list;

//    QFile file(file_info.filePath());
//    if (!file.exists() || !file.open(QFile::ReadOnly)) return pair_list;
//    QByteArray ini_data = file.readAll();
//    file.close();

//    QByteArray data(ini_data);
//    data.replace(QByteArrayLiteral("\r\n"),QByteArrayLiteral("\n"));
//    QByteArrayList data_list = data.split('\n');

//    bool in_current_section = false;
//    foreach (QByteArray row, data_list) {
//        if (row.contains(';')) {
//            row = row.left(row.indexOf(';')).trimmed();
//        }
//        if (row.isEmpty()) continue;

//        // section
//        if (row.startsWith('[') && row.endsWith(']') && row.length() > 2) {
//            if (in_current_section) break;
//            row.remove(row.length()-1,1);
//            row.remove(0,1);
//            if (row.toLower() == section.toLower()) {
//                in_current_section = true;
//            }
//            continue;
//        }

//        // value
//        if (!in_current_section) continue;
//        if (!row.contains('=')) continue;
//        if (row.isEmpty()) continue;

//        int index = row.indexOf('=');
//        QByteArray row_key = row.left(index);
//        QByteArray row_value = row.right(row.length()-1-index);
//        pair_list.append(QPair<QByteArray,QByteArray>(row_key,row_value));
//    }

//    return pair_list;
//}

QDir &Dta::baseDirPrivate()
{
    static QDir dir;
    return dir;
}

QMutex *Dta::baseDirMutex()
{
    static QMutex mutex;
    return &mutex;
}

QList<Dta::FP> &Dta::updaterListPrivate(Dta::FP new_fp)
{
    static QList<FP> list;
    if (new_fp) {
        QMutexLocker lock(updaterListMutex());
        if (!list.contains(new_fp)) list.append(new_fp);
    }
    return list;
}

QMutex *Dta::updaterListMutex()
{
    static QMutex mutex;
    return &mutex;
}

void Dta::updateAll()
{
    QMutexLocker lock(updaterListMutex());
    QList<FP> list = updaterListPrivate();
    foreach (FP fp, list) {
        if (!fp) continue;
        fp();
    }
}
