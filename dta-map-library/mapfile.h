#ifndef MAPFILE_H
#define MAPFILE_H

#include "mapitemmodel.h"

#include <QObject>
#include <QFileInfo>

class Section;

class DTAMAPLIBRARYSHARED_EXPORT MapFile : public QObject
{
    Q_OBJECT
public:
    explicit MapFile(QObject *parent = Q_NULLPTR);
    virtual ~MapFile();

    QFileInfo fileInfo() const { return m_FileInfo; }

    MapItemModel* model() { return m_Model; }

    // file i/o
    bool read(const QString& file_name) { return read(QFileInfo(file_name)); }
    bool read(const QFileInfo& file_info);

signals:

public slots:

private:
    QFileInfo m_FileInfo;

    MapItemModel* m_Model;
};

#endif // MAPFILE_H
