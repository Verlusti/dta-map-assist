#ifndef POSITION_H
#define POSITION_H

#include "dta-map-library_global.h"

#include <QObject>
#include <QVariant>

class DTAMAPLIBRARYSHARED_EXPORT Position
{
    Q_GADGET
public:
    enum Values
    {
        Invalid = 0
    };
    Q_ENUM(Values)

    Position() : m_X(Invalid), m_Y(Invalid) {}
    Position(const Position& other) : m_X(other.m_X), m_Y(other.m_Y) {}
    Position(const QVariant& value) : m_X(Invalid), m_Y(Invalid) { fromVariant(value); }
    Position(const int& value) : m_X(Invalid), m_Y(Invalid) { fromInt(value); }
    Position(const QByteArray& value) : m_X(Invalid), m_Y(Invalid) { fromByteArray(value); }
    virtual ~Position();

    bool isValid() const { return (m_X != Invalid && m_Y != Invalid); }

    int x() const { return m_X; }
    int y() const { return m_Y; }

    void fromVariant(const QVariant& value, bool* ok = Q_NULLPTR);
    void fromInt(const int& value, bool* ok = Q_NULLPTR);
    void fromByteArray(const QByteArray& value, bool* ok = Q_NULLPTR) { fromInt(value.toInt(),ok); }

    int toInt(bool* ok = Q_NULLPTR) const;
    QByteArray toByteArray(bool* ok = Q_NULLPTR) const;

    Position& operator=(const Position& value) { m_X = value.m_X; m_Y = value.m_Y; return *this; }
    Position& operator=(const QVariant& value) { fromVariant(value); return *this; }
    Position& operator=(const int& value) { fromInt(value); return *this; }
    Position& operator=(const QByteArray& value) { fromByteArray(value); return *this; }

    bool operator==(const Position& other) const { return (m_X == other.m_X && m_Y == other.m_Y); }
    bool operator!=(const Position& other) const { return !(m_X == other.m_X && m_Y == other.m_Y); }

    bool operator==(const int& other) const { return (Position(other) == *this); }
    bool operator!=(const int& other) const { return (Position(other) != *this); }

    bool operator==(const QByteArray& other) const { return (Position(other) == *this); }
    bool operator!=(const QByteArray& other) const { return (Position(other) != *this); }

    operator QVariant() const { return toInt(); }
    operator int() const { return toInt(); }
    operator QByteArray() const { return toByteArray(); }

private:
    int m_X;
    int m_Y;

};

inline bool operator==(const int& other, const Position& waypoint) { return (waypoint == other); }
inline bool operator!=(const int& other, const Position& waypoint) { return (waypoint != other); }

inline bool operator==(const QByteArray& other, const Position& waypoint) { return (waypoint == other); }
inline bool operator!=(const QByteArray& other, const Position& waypoint) { return (waypoint != other); }

#endif // POSITION_H
