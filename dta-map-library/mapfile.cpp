#include "mapfile.h"

#include "ini.h"

#include <QDebug>
#include <QFile>
#include <QByteArray>
#include <QMap>

MapFile::MapFile(QObject *parent) : QObject(parent)
{
    m_Model = new MapItemModel(this);
}

MapFile::~MapFile()
{

}

bool MapFile::read(const QFileInfo &file_info)
{
    return m_Model->import(Ini::fromFile(file_info));
}
