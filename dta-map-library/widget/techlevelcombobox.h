#ifndef TECHLEVELCOMBOBOX_H
#define TECHLEVELCOMBOBOX_H

#include "abstractenumcombobox.h"

class DTAMAPLIBRARYSHARED_EXPORT TechLevelComboBox : public AbstractEnumComboBox
{
    Q_OBJECT
public:
    explicit TechLevelComboBox(QWidget *parent = Q_NULLPTR) : AbstractEnumComboBox(parent) {}
    virtual ~TechLevelComboBox();

signals:

public slots:

protected:
    virtual QMetaEnum metaEnum() const;
    virtual int validMin() const;
    virtual int validMax() const;
    virtual int invalidValue() const;
};

#endif // TECHLEVELCOMBOBOX_H
