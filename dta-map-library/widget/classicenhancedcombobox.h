#ifndef CLASSICENHANCEDCOMBOBOX_H
#define CLASSICENHANCEDCOMBOBOX_H

#include "abstractenumcombobox.h"

class DTAMAPLIBRARYSHARED_EXPORT ClassicEnhancedComboBox : public AbstractEnumComboBox
{
    Q_OBJECT
public:
    explicit ClassicEnhancedComboBox(QWidget *parent = Q_NULLPTR) : AbstractEnumComboBox(parent) {}
    virtual ~ClassicEnhancedComboBox();

signals:

public slots:

protected:
    virtual QMetaEnum metaEnum() const;
    virtual int validMin() const;
    virtual int validMax() const;
    virtual int invalidValue() const;
};

#endif // CLASSICENHANCEDCOMBOBOX_H
