#ifndef WAYPOINTCOMBOBOX_H
#define WAYPOINTCOMBOBOX_H

#include "abstractidcombobox.h"
#include "waypoint.h"

class Waypoints;
class Option;

class DTAMAPLIBRARYSHARED_EXPORT WaypointComboBox : public AbstractIdComboBox
{
    Q_OBJECT
public:
    explicit WaypointComboBox(QWidget *parent = Q_NULLPTR) : AbstractIdComboBox(parent), m_WaypointsSection(Q_NULLPTR) {}
    explicit WaypointComboBox(Waypoints* waypoint_section = Q_NULLPTR, QWidget *parent = Q_NULLPTR) : AbstractIdComboBox(parent), m_WaypointsSection(Q_NULLPTR) { setWaypointsSection(waypoint_section); }
    explicit WaypointComboBox(bool invalid_id_allowed, QWidget *parent = Q_NULLPTR) : AbstractIdComboBox(invalid_id_allowed,parent), m_WaypointsSection(Q_NULLPTR) {}
    explicit WaypointComboBox(bool invalid_id_allowed, Waypoints* waypoint_section = Q_NULLPTR, QWidget *parent = Q_NULLPTR) : AbstractIdComboBox(invalid_id_allowed,parent), m_WaypointsSection(Q_NULLPTR) { setWaypointsSection(waypoint_section); }
public:

    virtual ~WaypointComboBox();

    virtual QList<int> availableIds() const;
    virtual bool isIdValid(const int& id) const;

    Waypoints* waypointsSection() { return m_WaypointsSection; }
    bool setWaypointsSection(Waypoints* waypoints_section);

signals:

public slots:

protected:
    virtual int invalidValue() const;
    virtual QString desc(const int& id) const;
    virtual QIcon icon(const int& id) const;

private:
    Waypoints* m_WaypointsSection;

    Waypoint waypoint(const int& id) const;

private slots:
    void onWaypointsSectionDestroyed() { m_WaypointsSection = Q_NULLPTR; }
    void onOptionAdded(Option* option);
    void onWaypointChanged(const Waypoint& waypoint);
};

#endif // WAYPOINTCOMBOBOX_H
