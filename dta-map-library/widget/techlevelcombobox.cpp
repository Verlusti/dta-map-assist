#include "techlevelcombobox.h"

//#include "section/forcedoptions.h"

TechLevelComboBox::~TechLevelComboBox()
{

}

QMetaEnum TechLevelComboBox::metaEnum() const
{
    return QMetaEnum();
//    return QMetaEnum::fromType<ForcedOptions::TechLevel>();
}

int TechLevelComboBox::validMin() const
{
    return 0;
//    return ForcedOptions::TechLevel7;
}

int TechLevelComboBox::validMax() const
{
    return 0;
//    return ForcedOptions::TechLevel1;
}

int TechLevelComboBox::invalidValue() const
{
    return 0;
//    return ForcedOptions::TechLevelUndefined;
}
