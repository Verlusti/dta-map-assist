#ifndef CREDITSCOMBOBOX_H
#define CREDITSCOMBOBOX_H

#include "abstractenumcombobox.h"

class DTAMAPLIBRARYSHARED_EXPORT CreditsComboBox : public AbstractEnumComboBox
{
    Q_OBJECT
public:
    explicit CreditsComboBox(QWidget *parent = Q_NULLPTR) : AbstractEnumComboBox(parent) {}
    virtual ~CreditsComboBox();

signals:

public slots:

protected:
    virtual QMetaEnum metaEnum() const;
    virtual int validMin() const;
    virtual int validMax() const;
    virtual int invalidValue() const;
};

#endif // CREDITSCOMBOBOX_H
