#include "tristatecheckbox.h"

#include <QDebug>

TristateCheckBox::TristateCheckBox(QWidget *parent) : QCheckBox(parent)
{
    connect(this,&QCheckBox::stateChanged,this,&TristateCheckBox::valueChanged);
}

TristateCheckBox::TristateCheckBox(const QString &text, QWidget *parent) : QCheckBox(text,parent)
{
    connect(this,&QCheckBox::stateChanged,this,&TristateCheckBox::valueChanged);
}

TristateCheckBox::~TristateCheckBox()
{

}

void TristateCheckBox::setValue(const Tristate &value)
{
    // prevent non-tristate to become tristate
    if (!isTristate()) {
        if (value != Tristate::True && value != Tristate::False) {
            emit(valueChanged(checkState()));
            return;
        }
    }
    setCheckState(value);
}
