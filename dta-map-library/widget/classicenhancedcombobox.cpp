#include "classicenhancedcombobox.h"

//#include "section/forcedoptions.h"

ClassicEnhancedComboBox::~ClassicEnhancedComboBox()
{

}

QMetaEnum ClassicEnhancedComboBox::metaEnum() const
{
    return QMetaEnum();
//    return QMetaEnum::fromType<ForcedOptions::GameType>();
}

int ClassicEnhancedComboBox::validMin() const
{
    return 0;
//    return ForcedOptions::Classic;
}

int ClassicEnhancedComboBox::validMax() const
{
    return 0;
    //return ForcedOptions::Enhanced;
}

int ClassicEnhancedComboBox::invalidValue() const
{
    return 0;
    //return ForcedOptions::GameTypeUndefined;
}
