#include "waypointcombobox.h"

#include "section/waypoints.h"
#include "option.h"
#include "option/waypointoption.h"

#include <QDebug>
#include <algorithm>

WaypointComboBox::~WaypointComboBox()
{

}

QList<int> WaypointComboBox::availableIds() const
{
    if (!m_WaypointsSection) return QList<int>();

    QList<int> list;
    foreach (WaypointOption* wp_opt, m_WaypointsSection->findChildren<WaypointOption*>(QString(),Qt::FindDirectChildrenOnly)) {
        if (!wp_opt) continue;
        list.append(wp_opt->waypoint().id());
    }
    std::sort(list.begin(),list.end());
    return list;
}

bool WaypointComboBox::isIdValid(const int &id) const
{
    return (id > Waypoint::Invalid);
}

bool WaypointComboBox::setWaypointsSection(Waypoints *waypoints_section)
{
    if (!waypoints_section) return false;
    m_WaypointsSection = waypoints_section;
    connect(m_WaypointsSection,&QObject::destroyed,this,&WaypointComboBox::onWaypointsSectionDestroyed,Qt::UniqueConnection);
    connect(m_WaypointsSection,&Waypoints::optionAdded,this,&WaypointComboBox::onOptionAdded);

    foreach (WaypointOption* wp_opt, m_WaypointsSection->findChildren<WaypointOption*>(QString(),Qt::FindDirectChildrenOnly)) {
        if (!wp_opt) continue;
        connect(wp_opt,&WaypointOption::waypointChanged,this,&WaypointComboBox::onWaypointChanged,Qt::UniqueConnection);
    }

    return true;
}

int WaypointComboBox::invalidValue() const
{
    return Waypoint::Invalid;
}

QString WaypointComboBox::desc(const int &id) const
{
    Waypoint wp = waypoint(id);
    QString desc_ = QStringLiteral("[%1] %2 (%3/%4)");
    return desc_.arg(wp.id()).arg(Waypoint::idToName(wp.id()).data()).arg(wp.x()).arg(wp.y());
}

QIcon WaypointComboBox::icon(const int &id) const
{
    return Waypoint::idToIcon(id);
}

Waypoint WaypointComboBox::waypoint(const int &id) const
{
    if (!m_WaypointsSection) return Waypoint();

    WaypointOption* wp_opt = qobject_cast<WaypointOption*>(m_WaypointsSection->option(id));
    if (!wp_opt) return Waypoint();

    return wp_opt->waypoint();
}

void WaypointComboBox::onOptionAdded(Option *option)
{
    WaypointOption* wp_opt = qobject_cast<WaypointOption*>(option);
    if (!wp_opt) return;
    connect(wp_opt,&WaypointOption::waypointChanged,this,&WaypointComboBox::onWaypointChanged,Qt::UniqueConnection);
}

void WaypointComboBox::onWaypointChanged(const Waypoint &waypoint)
{
    Q_UNUSED(waypoint);
    int current = currentId();
    clear();
    fill();
    setCurrentId(current);
}

