#ifndef ABSTRACTIDCOMBOBOX_H
#define ABSTRACTIDCOMBOBOX_H

#include "dta-map-library_global.h"

#include <QWidget>
#include <QComboBox>
#include <QIcon>

class DTAMAPLIBRARYSHARED_EXPORT AbstractIdComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit AbstractIdComboBox(QWidget* parent = Q_NULLPTR);
    explicit AbstractIdComboBox(bool invalid_id_allowed, QWidget* parent = Q_NULLPTR);
    virtual ~AbstractIdComboBox();

    bool isInverse() const { return m_Inverse; }

    int currentId() const;
    QList<int> usedIds() const;
    virtual QList<int> availableIds() const = 0; // override
    virtual bool isIdValid(const int& id) const = 0; // override

    bool isInvalidIdAllowed() const { return m_InvalidIdAllowed; }

    bool connectInverse(AbstractIdComboBox* inverse_box);

signals:
    void cleared();
    void filled();
    void added(const int& id);
    void removed(const int& id);
    void usedIdsChanged(const QList<int>& used_ids);
    void currentIdChanged(const int& id);

    // inverse signals
    void inverseAdded(const int& id);
    void inverseRemoved(const int& id);    

public slots:
    void clear();
    void fill();

    bool add(const int& id);
    bool remove(const int& id);
    bool removeCurrent();

    bool setCurrentId(const int& id);
    void setUsedIds(const QList<int>& used_ids);

protected:
    bool m_InvalidIdAllowed;

    void setInverse(bool inverse = true) { m_Inverse = inverse; }

    virtual int invalidValue() const = 0; // override
    virtual QString desc(const int& id) const = 0; // override
    virtual QIcon icon(const int& id) const = 0; // override

private:
    enum EmitMode
    {
        EmitSingle = 1 << 0,
        EmitInverse = 1 << 1,
        EmitList = 1 << 2
    };
    Q_DECLARE_FLAGS(EmitModes,EmitMode)

    bool m_Inverse;

    bool addPrivate(const int& id, const EmitModes& emit_modes);
    bool removePrivate(const int& id, const EmitModes& emit_modes);

private slots:
    void onCurrentIndexChanged(const int& index);
};

#endif // ABSTRACTIDCOMBOBOX_H
