#ifndef ENEMYHOUSEGROUPBOX_H
#define ENEMYHOUSEGROUPBOX_H

//#include "dta-map-library_global.h"

//#include <QMap>
//#include <QGroupBox>

//class Section;
//class Waypoints;
//class Option;
//class EnemyHouseOption;
//class TristateCheckBox;
//class SideComboBox;
//class ColorComboBox;
//class WaypointComboBox;

//class DTAMAPLIBRARYSHARED_EXPORT EnemyHouseGroupBox : public QGroupBox
//{
//    Q_OBJECT
//public:
//    explicit EnemyHouseGroupBox(QWidget *parent = Q_NULLPTR);
//    explicit EnemyHouseGroupBox(const QString& title, QWidget* parent = Q_NULLPTR);
//    virtual ~EnemyHouseGroupBox();

//    int addSectionOptions(Section* section);
//    bool addOption(Option* enemy_house_option);

//    Waypoints* waypointsSection() { return m_WaypointsSection; }
//    bool setWaypointsSection(Waypoints* waypoints_section);

//    TristateCheckBox* activeCheckBox(const int& enemy_house_id);
//    SideComboBox* sideComboBox(const int& enemy_house_id);
//    ColorComboBox* colorComboBox(const int& enemy_house_id);
//    WaypointComboBox* waypointComboBox(const int& enemy_house_id);

//signals:
//    void usedSidesExport(const QList<int>& used_sides);
//    void usedColorsExport(const QList<int>& used_colors);

//public slots:
//    void exportUsedSides();
//    void exportUsedColors();

//private:
//    Waypoints* m_WaypointsSection;

//    QWidget* widgetAtPosition(const int& row, const int& col);
//    bool addWidgetAtPosition(QWidget* widget, const int& row, const int& col);
//    void removeWidgetAtPosition(const int& row, const int& col);

//private slots:
//    void onWaypointsSectionDestroyed() { m_WaypointsSection = Q_NULLPTR; }
//};

#endif // ENEMYHOUSEGROUPBOX_H
