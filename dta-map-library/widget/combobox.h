#ifndef COMBOBOX_H
#define COMBOBOX_H

#include "sortfilterproxymodel.h"

#include <QComboBox>

class DTAMAPLIBRARYSHARED_EXPORT ComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit ComboBox(QWidget* parent = Q_NULLPTR);
    virtual ~ComboBox();

    bool setDataIndex(const QModelIndex& index);

    SortFilterProxyModel* proxyModel() { return m_ProxyModel; }

    bool setAllowed(const int& index, const bool& allowed = true) { return setAllowed(index,QVariant(allowed)); }
    bool unsetAllowed(const int& index) { return setAllowed(index,QVariant()); }
    bool setDisallowed(const int& index, const bool& disallowed = true) { return setAllowed(index,!disallowed); }
    bool unsetDisallowed(const int& index) { return setAllowed(index,QVariant()); }

signals:

public slots:
    void allowCurrent() { setAllowed(currentIndex()); }
    void disallowCurrent() { setDisallowed(currentIndex()); }

private:
    SortFilterProxyModel* m_ProxyModel;

    bool setAllowed(const int& index, const QVariant& allowed_value);
};

#endif // COMBOBOX_H
