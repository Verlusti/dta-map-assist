#ifndef TRISTATECHECKBOX_H
#define TRISTATECHECKBOX_H

#include "tristate.h"

#include <QCheckBox>

class DTAMAPLIBRARYSHARED_EXPORT TristateCheckBox : public QCheckBox
{
    Q_OBJECT
public:
    explicit TristateCheckBox(QWidget *parent = Q_NULLPTR);
    explicit TristateCheckBox(const QString& text, QWidget* parent = Q_NULLPTR);
    virtual ~TristateCheckBox();

    Tristate value() const { return checkState(); }

signals:
    void valueChanged(const Tristate& value);

public slots:
    void setValue(const Tristate& value);

};

#endif // TRISTATECHECKBOX_H
