#include "creditscombobox.h"

//#include "section/forcedoptions.h"

CreditsComboBox::~CreditsComboBox()
{

}

QMetaEnum CreditsComboBox::metaEnum() const
{
    return QMetaEnum();
    //return QMetaEnum::fromType<ForcedOptions::Credits>();
}

int CreditsComboBox::validMin() const
{
    return 0;
//    return ForcedOptions::Credits20000;
}

int CreditsComboBox::validMax() const
{
    return 0;
//    return ForcedOptions::Credits2500;
}

int CreditsComboBox::invalidValue() const
{
    return 0;
//    return ForcedOptions::CreditsUndefined;
}
