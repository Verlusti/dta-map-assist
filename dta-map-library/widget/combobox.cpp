#include "combobox.h"

#include "mapitemmodel.h"

ComboBox::ComboBox(QWidget *parent) : QComboBox(parent)
{
    m_ProxyModel = new SortFilterProxyModel(this);
    setModel(m_ProxyModel);
    m_ProxyModel->setSortRole(Item::KeyRole);
    m_ProxyModel->sort(0,Qt::AscendingOrder);
    m_ProxyModel->setDynamicSortFilter(true);
}

ComboBox::~ComboBox()
{

}

bool ComboBox::setDataIndex(const QModelIndex &index)
{
    if (!index.isValid()) return false;

    MapItemModel* model = qobject_cast<MapItemModel*>(const_cast<QAbstractItemModel*>(index.model()));
    if (!model) return false;
    m_ProxyModel->setSourceModel(model);
    m_ProxyModel->setSourceRootIndex(index);
    setRootModelIndex(m_ProxyModel->mapFromSource(index));
    setCurrentIndex(0);
    return true;
}

bool ComboBox::setAllowed(const int &index, const QVariant &allowed_value)
{
    if (index < 0 || index >= count()) return false;
    setItemData(index,allowed_value,Item::AllowedRole);
    return true;
}

