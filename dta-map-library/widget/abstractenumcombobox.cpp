#include "abstractenumcombobox.h"

AbstractEnumComboBox::~AbstractEnumComboBox()
{

}

QList<int> AbstractEnumComboBox::availableIds() const
{
    QList<int> list;
    QMetaEnum me = metaEnum();
    for (int x=0; x < me.keyCount(); x++) {
        list.append(me.value(x));
    }
    return list;
}

bool AbstractEnumComboBox::isIdValid(const int &id) const
{
    return (metaEnum().valueToKey(id));
}

QString AbstractEnumComboBox::desc(const int &id) const
{
    QMetaEnum me = metaEnum();
    if (id < validMin() || id > validMax()) return QStringLiteral("---");
    QString name = me.valueToKey(id);
    name.replace(me.name(),QStringLiteral(""));
    return name;
}

QIcon AbstractEnumComboBox::icon(const int &id) const
{
    Q_UNUSED(id);
    return QIcon();
}
