#include "enemyhousegroupbox.h"

//#include "section.h"
//#include "section/waypoints.h"

//#include "option/enemyhouseoption.h"
//#include "widget/tristatecheckbox.h"
//#include "widget/sidecombobox.h"
//#include "widget/colorcombobox.h"
//#include "widget/waypointcombobox.h"

//#include <QGridLayout>

//#define TEXT_ACTIVE "Active"

//#define COL_ACTIVE 0
//#define COL_SIDE 1
//#define COL_COLOR 2
//#define COL_WAYPOINT 3

//EnemyHouseGroupBox::EnemyHouseGroupBox(QWidget *parent) : QGroupBox (parent)
//{
//    m_WaypointsSection = Q_NULLPTR;
//    setLayout(new QGridLayout());
//}

//EnemyHouseGroupBox::EnemyHouseGroupBox(const QString &title, QWidget *parent) : QGroupBox (title,parent)
//{
//    m_WaypointsSection = Q_NULLPTR;
//    setLayout(new QGridLayout());
//}

//EnemyHouseGroupBox::~EnemyHouseGroupBox()
//{

//}

//int EnemyHouseGroupBox::addSectionOptions(Section *section)
//{
//    int added = 0;
//    if (!section) return added;

//    QList<EnemyHouseOption*> list = section->options<EnemyHouseOption*>();
//    foreach (EnemyHouseOption* eh, list) {
//        if (!eh) continue;
//        if (addOption(eh)) added++;
//    }

//    return added;
//}

//bool EnemyHouseGroupBox::addOption(Option *enemy_house_option)
//{
//    EnemyHouseOption* eh = qobject_cast<EnemyHouseOption*>(enemy_house_option);
//    if (!eh) return false;

//    int id = eh->id();

//    TristateCheckBox* active_box = new TristateCheckBox(QStringLiteral(TEXT_ACTIVE),this);
//    addWidgetAtPosition(active_box,id,COL_ACTIVE);
//    connect(active_box,&TristateCheckBox::toggled,eh,&EnemyHouseOption::setActive);
//    connect(eh,&EnemyHouseOption::activeChanged,active_box,&TristateCheckBox::setChecked);
//    active_box->setValue(eh->active());

//    SideComboBox* side_box = new SideComboBox(true,this);
//    side_box->fill();
//    addWidgetAtPosition(side_box,id,COL_SIDE);
//    connect(side_box,&SideComboBox::currentIdChanged,eh,&EnemyHouseOption::setSideId);
//    connect(eh,&EnemyHouseOption::sideIdChanged,side_box,&SideComboBox::setCurrentId);
//    connect(eh,&EnemyHouseOption::activeChanged,side_box,&SideComboBox::setEnabled);
//    side_box->setCurrentId(eh->side().id());
//    side_box->setEnabled(eh->active());

//    ColorComboBox* color_box = new ColorComboBox(true,this);
//    color_box->fill();
//    addWidgetAtPosition(color_box,id,COL_COLOR);
//    connect(color_box,&ColorComboBox::currentIdChanged,eh,&EnemyHouseOption::setColorId);
//    connect(eh,&EnemyHouseOption::colorIdChanged,color_box,&ColorComboBox::setCurrentId);
//    connect(eh,&EnemyHouseOption::activeChanged,color_box,&ColorComboBox::setEnabled);
//    color_box->setCurrentId(eh->color().id());
//    color_box->setEnabled(eh->active());

//    WaypointComboBox* wp_box = new WaypointComboBox(true,m_WaypointsSection,this);
//    wp_box->fill();
//    addWidgetAtPosition(wp_box,id,COL_WAYPOINT);
//    connect(wp_box,&WaypointComboBox::currentIdChanged,eh,&EnemyHouseOption::setWaypointId);
//    connect(eh,&EnemyHouseOption::waypointIdChanged,wp_box,&WaypointComboBox::setCurrentId);
//    connect(eh,&EnemyHouseOption::activeChanged,wp_box,&WaypointComboBox::setEnabled);
//    qDebug() << "EH WP ID" << eh->waypointId();
//    wp_box->setCurrentId(eh->waypointId());
//    wp_box->setEnabled(eh->active());

//    return true;
//}

//bool EnemyHouseGroupBox::setWaypointsSection(Waypoints *waypoints_section)
//{
//    if (!waypoints_section) return false;
//    m_WaypointsSection = waypoints_section;
//    connect(m_WaypointsSection,&QObject::destroyed,this,&EnemyHouseGroupBox::onWaypointsSectionDestroyed,Qt::UniqueConnection);

//    foreach (WaypointComboBox* wp_box, findChildren<WaypointComboBox*>(QString(),Qt::FindDirectChildrenOnly)) {
//        if (!wp_box) continue;
//        wp_box->setWaypointsSection(m_WaypointsSection);
//    }

//    return true;
//}

//TristateCheckBox *EnemyHouseGroupBox::activeCheckBox(const int &enemy_house_id)
//{
//    return qobject_cast<TristateCheckBox*>(widgetAtPosition(enemy_house_id,COL_ACTIVE));
//}

//SideComboBox *EnemyHouseGroupBox::sideComboBox(const int &enemy_house_id)
//{
//    return qobject_cast<SideComboBox*>(widgetAtPosition(enemy_house_id,COL_SIDE));
//}

//ColorComboBox *EnemyHouseGroupBox::colorComboBox(const int &enemy_house_id)
//{
//    return qobject_cast<ColorComboBox*>(widgetAtPosition(enemy_house_id,COL_COLOR));
//}

//WaypointComboBox *EnemyHouseGroupBox::waypointComboBox(const int &enemy_house_id)
//{
//    return qobject_cast<WaypointComboBox*>(widgetAtPosition(enemy_house_id,COL_WAYPOINT));
//}

//void EnemyHouseGroupBox::exportUsedSides()
//{
//    QList<int> list;
//    foreach (SideComboBox* side_box, findChildren<SideComboBox*>(QString(),Qt::FindDirectChildrenOnly)) {
//        if (!side_box) continue;
//        int id = side_box->currentId();
//        if (id >= 0 && !list.contains(id)) { list.append(id); }
//    }
//    emit(usedSidesExport(list));
//}

//void EnemyHouseGroupBox::exportUsedColors()
//{
//    QList<int> list;
//    foreach (ColorComboBox* color_box, findChildren<ColorComboBox*>(QString(),Qt::FindDirectChildrenOnly)) {
//        if (!color_box) continue;
//        int id = color_box->currentId();
//        if (id >= 0 && !list.contains(id)) { list.append(id); }
//    }
//    emit(usedColorsExport(list));
//}

//QWidget *EnemyHouseGroupBox::widgetAtPosition(const int &row, const int &col)
//{
//    QGridLayout* grid = qobject_cast<QGridLayout*>(layout());
//    if (!grid || row < 0 || col < 0) return Q_NULLPTR;
//    QLayoutItem* item = grid->itemAtPosition(row,col);
//    return (item) ? item->widget() : Q_NULLPTR;
//}

//bool EnemyHouseGroupBox::addWidgetAtPosition(QWidget *widget, const int &row, const int &col)
//{
//    if (!widget || row < 0 || col < 0) return false;
//    removeWidgetAtPosition(row,col);

//    QGridLayout* grid = qobject_cast<QGridLayout*>(layout());
//    if (!grid) return false;

//    widget->setParent(this);
//    grid->addWidget(widget,row,col);
//    return true;
//}

//void EnemyHouseGroupBox::removeWidgetAtPosition(const int &row, const int &col)
//{
//    QWidget* w = widgetAtPosition(row,col);
//    if (!w) return;

//    QGridLayout* grid = qobject_cast<QGridLayout*>(layout());
//    if (!grid) return;
//    grid->removeWidget(w);

//    w->disconnect();
//    w->deleteLater();
//}
