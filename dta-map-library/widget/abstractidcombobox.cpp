#include "abstractidcombobox.h"

#include <QDebug>

AbstractIdComboBox::AbstractIdComboBox(QWidget *parent) : QComboBox(parent), m_InvalidIdAllowed(false), m_Inverse(false)
{
    connect(this,QOverload<int>::of(&AbstractIdComboBox::currentIndexChanged),this,&AbstractIdComboBox::onCurrentIndexChanged);
}

AbstractIdComboBox::AbstractIdComboBox(bool invalid_id_allowed, QWidget *parent) : QComboBox(parent), m_InvalidIdAllowed(invalid_id_allowed), m_Inverse(false)
{
    connect(this,QOverload<int>::of(&AbstractIdComboBox::currentIndexChanged),this,&AbstractIdComboBox::onCurrentIndexChanged);
}

AbstractIdComboBox::~AbstractIdComboBox()
{

}

int AbstractIdComboBox::currentId() const
{
    bool ok = false;
    int retval = currentData().toInt(&ok);
    return (ok) ? retval : invalidValue();
}

QList<int> AbstractIdComboBox::usedIds() const
{
    QList<int> list;
    for (int x=0; x < count(); x++) {
        bool ok = false;
        int val = itemData(x).toInt(&ok);
        if (ok) list.append(val);
    }
    return list;
}

bool AbstractIdComboBox::connectInverse(AbstractIdComboBox *inverse_box)
{
    if (!inverse_box) return false;
    if (inverse_box == this) return false;

    setInverse(false);
    inverse_box->setInverse(true);

    // me -> inverse
    connect(this,&AbstractIdComboBox::inverseAdded,inverse_box,&AbstractIdComboBox::remove,Qt::UniqueConnection);
    connect(this,&AbstractIdComboBox::inverseRemoved,inverse_box,&AbstractIdComboBox::add,Qt::UniqueConnection);

    // inverse -> me
    connect(inverse_box,&AbstractIdComboBox::inverseAdded,this,&AbstractIdComboBox::remove,Qt::UniqueConnection);
    connect(inverse_box,&AbstractIdComboBox::inverseRemoved,this,&AbstractIdComboBox::add,Qt::UniqueConnection);

    return true;
}

void AbstractIdComboBox::clear()
{
    EmitModes emit_modes;
    emit_modes.setFlag(EmitSingle,true);
    emit_modes.setFlag(EmitInverse,true);
    emit_modes.setFlag(EmitList,true);

    foreach (int id, usedIds()) {
        removePrivate(id,emit_modes);
    }

    emit(cleared());
}

void AbstractIdComboBox::fill()
{
    EmitModes emit_modes;
    emit_modes.setFlag(EmitSingle,true);
    emit_modes.setFlag(EmitInverse,true);
    emit_modes.setFlag(EmitList,true);

    foreach (int id, availableIds()) {
        addPrivate(id,emit_modes);
    }

    setCurrentIndex(0);

    emit(filled());
}

bool AbstractIdComboBox::add(const int &id)
{
    EmitModes emit_modes;
    emit_modes.setFlag(EmitSingle,true);
    emit_modes.setFlag(EmitInverse,(!qobject_cast<AbstractIdComboBox*>(sender())));
    emit_modes.setFlag(EmitList,true);

    return addPrivate(id,emit_modes);
}

bool AbstractIdComboBox::remove(const int &id)
{
    EmitModes emit_modes;
    emit_modes.setFlag(EmitSingle,true);
    emit_modes.setFlag(EmitInverse,(!qobject_cast<AbstractIdComboBox*>(sender())));
    emit_modes.setFlag(EmitList,true);

    return removePrivate(id,emit_modes);
}

bool AbstractIdComboBox::removeCurrent()
{
    QVariant current = currentData();
    if (!current.isValid()) return false;
    return remove(current.toInt());
}

bool AbstractIdComboBox::setCurrentId(const int &id)
{
    int index = findData(id);
    if (index < 0) return false;
    setCurrentIndex(index);
    return true;
}

void AbstractIdComboBox::setUsedIds(const QList<int> &used_ids)
{
    EmitModes emit_modes;
    emit_modes.setFlag(EmitSingle,true);
    emit_modes.setFlag(EmitInverse,true);
    emit_modes.setFlag(EmitList,false);

    QList<int> current_used_ids = usedIds();
    bool changed = false;
    foreach (int id, current_used_ids) {
        if (!used_ids.contains(id)) {
            if (removePrivate(id,emit_modes)) changed = true;
        }
    }
    foreach (int id, used_ids) {
        if (addPrivate(id,emit_modes)) changed = true;
    }
    if (changed) emit (usedIdsChanged(usedIds()));
}

bool AbstractIdComboBox::addPrivate(const int& id, const EmitModes &emit_modes)
{
    int index = findData(id);
    if (index >= 0) return false;
    if (!isIdValid(id)) return false;

    bool set = false;
    for (int x=0; x < count(); x++) {
        if (id <= itemData(x).toInt()) {
            insertItem(x,icon(id),desc(id),id);
            setCurrentIndex(x);
            set = true;
            break;
        }
    }

    if (!set) {
        addItem(icon(id),desc(id),id);
        setCurrentIndex(findData(id));
    }

    if (emit_modes.testFlag(EmitSingle)) emit(added(id));
    if (emit_modes.testFlag(EmitInverse)) emit(inverseAdded(id));
    if (emit_modes.testFlag(EmitList)) emit (usedIdsChanged(usedIds()));
    return true;
}

bool AbstractIdComboBox::removePrivate(const int &id, const EmitModes &emit_modes)
{
    int index = findData(id);
    if (index < 0) return false;

    removeItem(index);

    if (emit_modes.testFlag(EmitSingle)) emit(removed(id));
    if (emit_modes.testFlag(EmitInverse)) emit(inverseRemoved(id));
    if (emit_modes.testFlag(EmitList)) emit (usedIdsChanged(usedIds()));
    return true;
}

void AbstractIdComboBox::onCurrentIndexChanged(const int &index)
{
    int current_id = itemData(index).toInt();
    emit (currentIdChanged(current_id));
}
