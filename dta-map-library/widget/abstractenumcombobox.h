#ifndef ABSTRACTENUMCOMBOBOX_H
#define ABSTRACTENUMCOMBOBOX_H

#include "abstractidcombobox.h"

#include <QMetaEnum>

class DTAMAPLIBRARYSHARED_EXPORT AbstractEnumComboBox : public AbstractIdComboBox
{
    Q_OBJECT
public:
    explicit AbstractEnumComboBox(QWidget *parent = Q_NULLPTR) : AbstractIdComboBox(parent) {}
private: explicit AbstractEnumComboBox(bool invalid_id_allowed, QWidget *parent = Q_NULLPTR) : AbstractIdComboBox(invalid_id_allowed,parent) {}
public:
    virtual ~AbstractEnumComboBox();

    virtual QList<int> availableIds() const;
    virtual bool isIdValid(const int& id) const;

signals:

public slots:
    virtual QString desc(const int& id) const;
    virtual QIcon icon(const int& id) const;

protected:
    virtual QMetaEnum metaEnum() const = 0; // override
    virtual int validMin() const = 0; // override
    virtual int validMax() const = 0; // override
};

#endif // ABSTRACTENUMCOMBOBOX_H
