#include "section.h"

#include <QMutex>
#include <QMutexLocker>

#include <QLineEdit>
#include <QSpinBox>

#include "widget/tristatecheckbox.h"
#include "widget/sidecombobox.h"
#include "widget/colorcombobox.h"

Section::Section(QObject *parent) : QObject(parent)
{

}

Section::~Section()
{

}

QByteArrayList Section::registeredClasses()
{
    QMutexLocker lock(registeredClassesMutex());
    return registeredClassesMap().keys();
}

Section *Section::create(const QByteArray &section_name, QObject *parent)
{
    QMutexLocker lock(registeredClassesMutex());
    Section* section = registeredClassesMap().value(section_name,Q_NULLPTR);
    if (section) return section->create(parent);
    return Q_NULLPTR;
}

int Section::createStandardSections(QObject *parent)
{
    if (!parent) return -1;
    int count = 0;
    foreach (QByteArray section_name, registeredClasses()) {
        if (sectionType(section_name) != StandardType) continue;
        create(section_name,parent);
        count++;
    }
    return count;
}

Section::SectionType Section::sectionType(const QByteArray &section_name)
{
    QMutexLocker lock(registeredClassesMutex());
    Section* section = registeredClassesMap().value(section_name,Q_NULLPTR);
    if (!section) return NoType;
    return section->sectionType();
}

Section::KeyType Section::keyType(const QByteArray &section_name)
{
    QMutexLocker lock(registeredClassesMutex());
    Section* section = registeredClassesMap().value(section_name,Q_NULLPTR);
    if (!section) return NoKey;
    return section->keyType();
}

void Section::clear()
{

    m_BinaryData.clear();
    if (!signalsBlocked()) emit (cleared());
}

bool Section::fromIni(const QByteArray &ini_data)
{
    clear();
    if (ini_data.isEmpty()) return false;

    QByteArrayList list = ini_data.split('\n');
    foreach (QByteArray row, list) {
        row = row.trimmed();
        if (row.contains(';')) {
            row = row.left(row.indexOf(';')).trimmed();
        }
        if (row.isEmpty()) continue;
        if (!row.contains('=')) continue;

        int index = row.indexOf('=');
        QByteArray row_key = row.left(index);
        QByteArray row_value = row.right(row.length()-1-index);

        Option* opt = option(row_key);
        if (opt) {
            opt->valueImport(row_value);
        }
    }

//    if (valueType() == Binary) {
//        m_Binary = QByteArray::fromBase64(m_Binary);
//        m_Data.clear();
//    }

    if (!signalsBlocked()) emit(readFinished());

    qDebug() << "read finished." << m_Options.count();

    if (metaObject()->className() == QString("Waypoints")) {
//        foreach (Option* o, m_Options) {
//            qDebug() << o->valueType();
//        }
    }

    return true;
}

QByteArray Section::toIni() const
{
    QByteArray section_data;

    return section_data;
}

Option *Section::option(const QByteArray &key)
{
    if (key.isEmpty()) return Q_NULLPTR;
    if (keyType() == EnumKey) {
        int enum_value = metaEnum().keyToValue(key);
        if (enum_value >= 0) return option(enum_value);
    } else if (keyType() == IntKey) {
        bool ok = false;
        int int_key = key.toInt(&ok);
        if (ok) return option(int_key);
    }
    Option* opt = m_Options.value(key,Q_NULLPTR);
    if (!opt) {
        addOption(key,defaultValueType());
        opt = m_Options.value(key,Q_NULLPTR);
    }
    return opt;
}

Option *Section::option(const int &key)
{
    Option* opt = m_Options.value(key,Q_NULLPTR);
    if (!opt) {
        addOption(key,defaultValueType());
        opt = m_Options.value(key,Q_NULLPTR);
    }
    return opt;
}

bool Section::connectWidget(QWidget *widget, Option *option)
{
    if (!option || !widget) return false;

    QLineEdit* line_edit = qobject_cast<QLineEdit*>(widget);
    if (line_edit) {
        connect(line_edit,&QLineEdit::textChanged,option,&Option::setString,Qt::UniqueConnection);
        connect(option,&Option::stringChanged,line_edit,&QLineEdit::setText,Qt::UniqueConnection);
        connect(option,&Option::cleared,line_edit,&QLineEdit::clear,Qt::UniqueConnection);
        return true;
    }

    QSpinBox* spin_box = qobject_cast<QSpinBox*>(widget);
    if (spin_box) {
        connect(spin_box,QOverload<int>::of(&QSpinBox::valueChanged),option,&Option::setInt,Qt::UniqueConnection);
        connect(option,&Option::intChanged,spin_box,&QSpinBox::setValue,Qt::UniqueConnection);
        return true;
    }

    TristateCheckBox* tri_box = qobject_cast<TristateCheckBox*>(widget);
    if (tri_box) {
        connect(tri_box,&TristateCheckBox::valueChanged,option,&Option::setTristate,Qt::UniqueConnection);
        connect(option,&Option::tristateChanged,tri_box,&TristateCheckBox::setValue,Qt::UniqueConnection);
        return true;
    }

    AbstractIdComboBox* abstract_box = qobject_cast<AbstractIdComboBox*>(widget);
    if (abstract_box) {
        connect(abstract_box,&AbstractIdComboBox::usedIdsChanged,option,&Option::setIntList,Qt::UniqueConnection);
        connect(option,&Option::intListChanged,abstract_box,&AbstractIdComboBox::setUsedIds,Qt::UniqueConnection);
        connect(abstract_box,&AbstractIdComboBox::currentIdChanged,option,&Option::setInt,Qt::UniqueConnection);
        connect(option,&Option::intChanged,abstract_box,&AbstractIdComboBox::setCurrentId,Qt::UniqueConnection);
        return true;
    }

    return false;
}

void Section::registerClass(Section *section)
{
    if (!section) return;
    QMutexLocker lock(registeredClassesMutex());
    if (!registeredClassesMap().contains(section->metaObject()->className())) {
        registeredClassesMap().insert(section->metaObject()->className(),section);
    }
}

bool Section::addOption(const QVariant& key, Option *option)
{
    if (!option) return false;
    if (!key.isValid()) return false;
    if (m_Options.contains(key)) return false;
    m_Options.insert(key,option);
    option->setParent(this);
    option->setup();   
    emit(optionAdded(option));
    return true;
}

bool Section::addOption(const QVariant &key, const Option::ValueType &value_type, Tristate::OutputFormat tristate_output_format)
{
    Option* option = createDefaultOption(value_type);
    if (!option) return false;

    if (value_type == Option::TristateValue) option->setTristateOutputFormat(tristate_output_format);
    bool ret = addOption(key,option);
    if (!ret) option->deleteLater();
    return ret;
}

bool Section::addStandardOption(const QVariant &key, Option *option)
{
    if (!option) return false;
    option->setStandard(true);
    return addOption(key,option);
}

bool Section::addStandardOption(const QVariant &key, const Option::ValueType &value_type, Tristate::OutputFormat tristate_output_format)
{
    Option* option = createDefaultOption(value_type);
    if (!option) return false;

    if (value_type == Option::TristateValue) option->setTristateOutputFormat(tristate_output_format);
    bool ret = addStandardOption(key,option);
    if (!ret) option->deleteLater();
    return ret;
}

Option *Section::createDefaultOption(const Option::ValueType &value_type)
{
    QObject* obj = defaultOptionMetaObject().newInstance();
    if (!obj) return Q_NULLPTR;

    Option* option = qobject_cast<Option*>(obj);
    if (!option) {
        obj->deleteLater();
        return Q_NULLPTR;
    }
    option->setValueType(value_type);
    return option;
}

QMutex *Section::registeredClassesMutex()
{
    static QMutex mutex;
    return &mutex;
}

QMap<QByteArray, Section *> &Section::registeredClassesMap()
{
    static QMap<QByteArray, Section*> map;
    return map;
}

