#include "waypoint.h"

#include <QMetaEnum>

#define ICON_INVALID ":/icons/waypoint_invalid.png"
#define ICON_GENERIC ":/icons/current-location.png"
#define ICON_PLAYER ":/icons/waypoint-%1.png"
#define ICON_HOME ":/icons/structural.png"
#define ICON_ALT_HOME ":/icons/structural-alt.png"

Waypoint::Waypoint() : m_Id(Invalid)
{

}

Waypoint::~Waypoint()
{

}

bool Waypoint::setPosition(const int &position)
{
    bool ok = false;
    m_Position.fromInt(position,&ok);
    return ok;
}

QByteArray Waypoint::idToName(const int &id)
{
    int id_ = (id >= 0) ? id : Invalid;
    QByteArray name = QMetaEnum::fromType<Id>().valueToKey(id_);
    if (name.isEmpty()) name = staticMetaObject.className();
    return name;
}

QPixmap Waypoint::idToPixmap(const int &id)
{
    int id_ = (id >= 0) ? id : Invalid;
    if (id_ == Invalid) return QPixmap(QStringLiteral(ICON_INVALID));
    if (id >= Player1 && id <= Player8) return QPixmap(QString(QStringLiteral(ICON_PLAYER)).arg(id+1));
    if (id == DefaultHomeCell) return QPixmap(QStringLiteral(ICON_HOME));
    if (id == DefaultAltHomeCell) return QPixmap(QStringLiteral(ICON_ALT_HOME));
    return QPixmap(ICON_GENERIC);
}
