#include "position.h"

Position::~Position()
{

}

void Position::fromVariant(const QVariant &value, bool *ok)
{
    fromInt(value.toInt(),ok);
}

void Position::fromInt(const int &value, bool *ok)
{
    if (value < 1001) {
        m_X = Invalid;
        m_Y = Invalid;
        if (ok) *ok = false;
        return;
    }

    m_X = value / 1000;
    m_Y = value % 1000;
    if (ok) *ok = true;
}

int Position::toInt(bool *ok) const
{
    if (!isValid()) {
        if (ok) *ok = false;
        return 0;
    }
    if (ok) *ok = true;
    return (m_X * 1000) + m_Y;
}

QByteArray Position::toByteArray(bool *ok) const
{
    if (!isValid()) {
        if (ok) *ok = false;
        return QByteArray();
    }
    if (ok) *ok = true;
    return QByteArray::number(toInt());
}

