#ifndef WAYPOINTOPTION_H
#define WAYPOINTOPTION_H

#include "option.h"
#include "waypoint.h"

#include <QDebug>

class DTAMAPLIBRARYSHARED_EXPORT WaypointOption : public Option
{
    Q_OBJECT
public:
    Q_INVOKABLE explicit WaypointOption(QObject *parent = Q_NULLPTR);
private: explicit WaypointOption(const ValueType& value_type, QObject *parent = Q_NULLPTR);
public:
    virtual ~WaypointOption();

    virtual void setValueType(const ValueType& value_type) { Q_UNUSED(value_type); Option::setValueType(IntValue); }

    virtual int toInt(bool* ok = Q_NULLPTR) const;

    Waypoint waypoint() const { return m_Waypoint; }

signals:
    void waypointChanged(const Waypoint& waypoint);

public slots:
    virtual bool setInt(const int& value);

protected:
    virtual void setup();

private:
    Waypoint m_Waypoint;
};

#endif // WAYPOINTOPTION_H
