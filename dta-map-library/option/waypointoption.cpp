#include "waypointoption.h"

#include "section.h"

#include <QDebug>

WaypointOption::WaypointOption(QObject *parent) : Option(IntValue,parent)
{

}

WaypointOption::WaypointOption(const Option::ValueType &value_type, QObject *parent) : Option(IntValue,parent)
{
    Q_UNUSED(value_type);
}

WaypointOption::~WaypointOption()
{

}

int WaypointOption::toInt(bool *ok) const
{
    return m_Waypoint.position().toInt(ok);
}

bool WaypointOption::setInt(const int &value)
{
    bool ret = m_Waypoint.setPosition(value);
    if (ret) emit(waypointChanged(m_Waypoint));
    return ret;
}

void WaypointOption::setup()
{
    Section* section = qobject_cast<Section*>(parent());
    if (!section) return;

    QVariant var = section->optionKey(this);
    bool ok = false;
    int id = var.toInt(&ok);
    if (ok) m_Waypoint.setId(id);
}
