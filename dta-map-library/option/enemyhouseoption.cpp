#include "enemyhouseoption.h"

//#include "waypoint.h"

//#include <algorithm>

//EnemyHouseOption::EnemyHouseOption(QObject *parent) : Option(IntListValue,parent)
//{
//    m_EnumId = -1;
//    m_Active = false;
//    m_WaypointId = Waypoint::Invalid;
//}

//EnemyHouseOption::EnemyHouseOption(const int &enum_id, QObject *parent) : Option (IntListValue,parent)
//{
//    m_EnumId = enum_id;
//    m_Active = false;
//    m_WaypointId = Waypoint::Invalid;
//}

//EnemyHouseOption::EnemyHouseOption(const Option::ValueType &value_type, QObject *parent) : Option(IntListValue,parent)
//{
//    Q_UNUSED(value_type);
//    m_EnumId = -1;
//    m_Active = false;
//    m_WaypointId = Waypoint::Invalid;
//}

//EnemyHouseOption::~EnemyHouseOption()
//{

//}

//IntList EnemyHouseOption::toIntList(bool *ok) const
//{
//    if (ok) *ok = true;
//    IntList list;

//    return list;
//}

//bool EnemyHouseOption::setIntList(const IntList &value)
//{
//    if (!value.isEmpty() && value.count() != 3) return false;

//    if (value.isEmpty()) {
//        setActive(false);
//        setSideId(Side::Invalid);
//        setColorId(Color::InvalidValue);
//        setWaypointId(Waypoint::Invalid);
//    } else {
//        setActive(true);
//        setSideId(value.value(0));
//        setColorId(value.value(1));
//        setWaypointId(value.value(2));
//    }

//    return true;
//}

//void EnemyHouseOption::setActive(const bool &value)
//{
//    if (value != m_Active) {
//        m_Active = value;
//        emit(activeChanged(m_Active));
//    }

//    if (m_Active) {
//        // enable before me
//        EnemyHouseOption* eh = enemyHouseOptionById(id()-1);
//        if (eh) eh->setActive(true);
//    } else {
//        EnemyHouseOption* eh = enemyHouseOptionById(id()+1);
//        if (eh) eh->setActive(false);
//    }
//}

//void EnemyHouseOption::setSide(const Side &side)
//{
//    if (m_Side == side) return;
//    m_Side = side;
//    emit(sideChanged(m_Side));
//    emit(sideIdChanged(m_Side));
//}

//void EnemyHouseOption::setColor(const Color &color)
//{
//    if (m_Color == color) return;
//    m_Color = color;
//    emit(colorChanged(m_Color));
//    emit(colorIdChanged(m_Color));
//}

//void EnemyHouseOption::setWaypointId(const int &waypoint_id)
//{
//    if (m_WaypointId == waypoint_id) return;
//    m_WaypointId = waypoint_id;
//    emit(waypointIdChanged(m_WaypointId));
//}

//void EnemyHouseOption::setup()
//{
//    if (m_EnumId < 0 || !parent()) return;
//    m_DataPtr.clear();
//    QList<EnemyHouseOption*> list = parent()->findChildren<EnemyHouseOption*>(QString(),Qt::FindDirectChildrenOnly);
//    QList<int> id_list;
//    foreach (EnemyHouseOption* eh, list) {
//        if (!eh) continue;
//        if (m_DataPtr.isNull() && !eh->m_DataPtr.isNull()) {
//            m_DataPtr = eh->m_DataPtr;
//        }
//        id_list.append(eh->m_EnumId);
//    }
//    if (m_DataPtr.isNull()) {
//        m_DataPtr = DataPtr(new Data());
//    }

//    std::sort(id_list.begin(),id_list.end());
//    m_DataPtr.data()->m_EnumIdList = id_list;
//}

//EnemyHouseOption *EnemyHouseOption::enemyHouseOptionById(const int &id)
//{
//    if (id < 0) return Q_NULLPTR;
//    if (!m_DataPtr) return Q_NULLPTR;
//    QList<EnemyHouseOption*> list = parent()->findChildren<EnemyHouseOption*>(QString(),Qt::FindDirectChildrenOnly);
//    foreach (EnemyHouseOption* eh, list) {
//        if (!eh) continue;
//        if (eh->id() == id) return eh;
//    }

//    return Q_NULLPTR;
//}

