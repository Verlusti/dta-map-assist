#ifndef ENEMYHOUSEOPTION_H
#define ENEMYHOUSEOPTION_H

//#include "option.h"
//#include "side.h"
//#include "color.h"

//#include <QSharedPointer>

//class DTAMAPLIBRARYSHARED_EXPORT EnemyHouseOption : public Option
//{
//    Q_OBJECT
//public:
//    Q_INVOKABLE explicit EnemyHouseOption(QObject *parent = Q_NULLPTR);
//    explicit EnemyHouseOption(const int& enum_id, QObject* parent = Q_NULLPTR);
//private: explicit EnemyHouseOption(const ValueType& value_type, QObject *parent = Q_NULLPTR);
//public:
//    virtual ~EnemyHouseOption();

//    virtual void setValueType(const ValueType& value_type) { Q_UNUSED(value_type); Option::setValueType(IntListValue); }

//    int enumId() const { return m_EnumId; }
//    int id() const { return (!m_DataPtr.isNull()) ? m_DataPtr.data()->m_EnumIdList.indexOf(m_EnumId) : -1; }

//    bool active() const { return m_Active; }
//    Side side() const { return m_Side; }
//    Color color() const { return m_Color; }
//    int waypointId() const { return m_WaypointId; }

//    virtual IntList toIntList(bool* ok = Q_NULLPTR) const;

//signals:
//    void activeChanged(const bool& value);
//    void sideChanged(const Side& side);
//    void sideIdChanged(const int& side_id);
//    void colorChanged(const Color& color);
//    void colorIdChanged(const int& color_id);
//    void waypointIdChanged(const int& waypoint_id);

//public slots:
//    virtual bool setIntList(const IntList& value);

//    void setActive(const bool& value);
//    void setSide(const Side& side);
//    void setSideId(const int& side_id) { setSide(side_id); }
//    void setColor(const Color& color);
//    void setColorId(const int& color_id) { setColor(color_id); }
//    void setWaypointId(const int& waypoint_id);

//protected:
//    virtual void setup();

//private:
//    struct Data {
//        QList<int> m_EnumIdList;
//    };
//    typedef QSharedPointer<Data> DataPtr;

//    int m_EnumId;
//    DataPtr m_DataPtr;

//    bool m_Active;
//    Side m_Side;
//    Color m_Color;
//    int m_WaypointId;

//    EnemyHouseOption* enemyHouseOptionById(const int& id);

//private slots:

//};

#endif // ENEMYHOUSEOPTION_H
