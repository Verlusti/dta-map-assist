#ifndef INI_H
#define INI_H

#include "dta-map-library_global.h"

#include <QList>
#include <QMap>
#include <QFileInfo>

class DTAMAPLIBRARYSHARED_EXPORT Ini
{
public:
    class SectionData
    {
    public:
        SectionData();
        SectionData(const SectionData& other) { *this = other; }
        virtual ~SectionData();

        void clear();

        QList<QByteArray> keys() const { return m_Keys; }
        QByteArray value(const QByteArray& key) const { QByteArray* ba = m_Values.value(key,Q_NULLPTR); return (ba) ? *ba : QByteArray(); }

        void set(const QByteArray& key, const QByteArray& value);
        void unset(const QByteArray& key);

        SectionData& operator=(const SectionData& other);

    private:
        QList<QByteArray> m_Keys;
        QMap<QByteArray,QByteArray*> m_Values;
    };

    Ini();
    Ini(const Ini& other) { *this = other; }
    virtual ~Ini();

    void clear();

    QList<QByteArray> names() const { return m_SectionNames; }
    SectionData* data(const QByteArray& name) const { return m_SectionData.value(name,Q_NULLPTR); }
    SectionData* add(const QByteArray& name);
    void remove(const QByteArray& name);

    // byte array i/o
    static Ini fromRawData(const QByteArray& ini_data);
    static Ini fromFile(const QFileInfo& ini_file_info);
    static Ini fromFile(const QString& ini_file_name) { return fromFile(QFileInfo(ini_file_name)); }
//    QByteArray toIni() const;

    Ini& operator=(const Ini& other);

private:
    QList<QByteArray> m_SectionNames;
    QMap<QByteArray,SectionData*> m_SectionData;
};

#endif // INI_H
