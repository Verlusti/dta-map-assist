#ifndef SORTFILTERPROXYMODEL_H
#define SORTFILTERPROXYMODEL_H

#include "dta-map-library_global.h"

#include <QSortFilterProxyModel>

class DTAMAPLIBRARYSHARED_EXPORT SortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    SortFilterProxyModel(QObject *parent = Q_NULLPTR);
    virtual ~SortFilterProxyModel() override;

    void setRootIndex(const QModelIndex& root_index) { setSourceRootIndex(mapToSource(root_index)); }
    void setSourceRootIndex(const QModelIndex& source_root_index) { m_SourceRootIndex = source_root_index; invalidateFilter(); }

    bool filterInvalidKey() const { return m_FilterInvalidKey; }
    void setFilterInvalidKey(const bool& filter_invalid_key = true);
    void unsetFilterInvalidKey() { setFilterInvalidKey(false); }

    QVariant filterAllowed() const { return m_FilterAllowed; }
    void setFilterAllowed(const bool& allowed);
    void unsetFilterAllowed();

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
//    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    QModelIndex m_SourceRootIndex;

    bool m_FilterInvalidKey;
    QVariant m_FilterAllowed;


};

#endif // SORTFILTERPROXYMODEL_H
