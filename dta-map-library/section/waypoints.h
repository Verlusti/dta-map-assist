#ifndef WAYPOINTS_H
#define WAYPOINTS_H

#include "section.h"

class DTAMAPLIBRARYSHARED_EXPORT Waypoints : public SectionT<Waypoints>
{
    Q_OBJECT
public:
    enum Options {};
    Q_ENUM(Options)

    explicit Waypoints(QObject *parent = Q_NULLPTR);
    virtual ~Waypoints();

    // overrides
    virtual SectionType sectionType() const { return StandardType; }
    virtual KeyType keyType() const { return IntKey; }
    virtual Option::ValueType defaultValueType() const { return Option::IntValue; }
    virtual QMetaObject defaultOptionMetaObject() const;

signals:

public slots:
};

#endif // WAYPOINTS_H
