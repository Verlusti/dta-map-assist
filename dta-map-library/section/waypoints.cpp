#include "waypoints.h"

#include "option/waypointoption.h"

static SectionT<Waypoints>::Register reg;

Waypoints::Waypoints(QObject *parent) : SectionT<Waypoints>(parent)
{

}

Waypoints::~Waypoints()
{

}

QMetaObject Waypoints::defaultOptionMetaObject() const
{
    return WaypointOption::staticMetaObject;
}
