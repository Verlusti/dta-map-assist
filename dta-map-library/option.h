#ifndef OPTION_H
#define OPTION_H

#include "tristate.h"
#include "position.h"

#include <QObject>
#include <QVariant>

class Section;

typedef QList<int> IntList;

class DTAMAPLIBRARYSHARED_EXPORT Option : public QObject
{
    Q_OBJECT
public:
    enum ValueType {
        NoValue,
        ByteArrayValue,
        IntValue,
        DoubleValue,
        TristateValue,
        IntListValue,
        VariantListValue,
        CustomValue
    };
    Q_ENUM(ValueType)

    Q_INVOKABLE explicit Option(QObject *parent = Q_NULLPTR);
    explicit Option(const ValueType& value_type, QObject *parent = Q_NULLPTR);
    virtual ~Option();

    bool standard() const { return m_Standard; }

    ValueType valueType() const { return m_ValueType; }
    virtual void setValueType(const ValueType& value_type) { m_ValueType = value_type; }
    bool isValueTypeValid() const { return (m_ValueType != NoValue); }

    QVariant value() const { return m_Value; }
    virtual bool isValid() const;

    virtual QByteArray toByteArray(bool* ok = Q_NULLPTR) const;
    virtual QString toString(bool* ok = Q_NULLPTR) const;
    virtual int toInt(bool* ok = Q_NULLPTR) const;
    virtual double toDouble(bool* ok = Q_NULLPTR) const;
    virtual Tristate toTristate(bool* ok = Q_NULLPTR) const;
    virtual Qt::CheckState toCheckState(bool* ok = Q_NULLPTR) const;
    virtual IntList toIntList(bool* ok = Q_NULLPTR) const;

signals:
    void cleared();
    void byteArrayChanged(const QByteArray& value);
    void stringChanged(const QString& value);
    void intChanged(const int& value);
    void doubleChanged(const double& value);
    void tristateChanged(const Tristate& value);
    void checkStateChanged(const Qt::CheckState& value);
    void intListChanged(const IntList& value);

public slots:
    virtual void clear();
    virtual void emitCurrentValue();

    virtual bool setByteArray(const QByteArray& value);
    virtual bool setString(const QString& value) { return setByteArray(value.toUtf8()); }
    virtual bool setInt(const int& value);
    virtual bool setDouble(const double& value);
    virtual bool setTristate(const Tristate& value);
    virtual bool setTristateOutputFormat(const Tristate::OutputFormat& output_format);
    virtual bool setCheckState(const Qt::CheckState& value) { return setTristate(value); }
    virtual bool setIntList(const IntList& value);

protected:
    virtual void setup() {} // override
    virtual bool valueImport(const QByteArray value); // override
    virtual QByteArray valueExport(bool* ok = Q_NULLPTR); // override

private:
    bool m_Standard;
    ValueType m_ValueType;
    QVariant m_Value;
    Tristate m_TristateValue;

    friend class Section;
    void setStandard(const bool& standard) { m_Standard = standard; }

};

#endif // OPTION_H
