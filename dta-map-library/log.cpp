#include "log.h"

#include <QDebug>
#include <QMutex>
#include <QMutexLocker>
#include <QtGlobal>
#include <QThread>

#include <cstdio>

#ifdef Q_OS_LINUX
#include <syslog.h>
#endif

#define OUTPUT_DEVICE stdout

Log::Log()
{

}

Log::~Log()
{

}

void Log::installMessageHandler(bool enable)
{
    if (enable) {
        qInstallMessageHandler(Log::messageHandler);
    } else {
        qInstallMessageHandler(0);
    }
}

bool Log::syslogSupport()
{
#ifdef Q_OS_LINUX
    return true;
#endif
    return false;
}

Log::Systems Log::systems()
{
    QMutexLocker lock(systemsMutex());
    Q_UNUSED(lock);
    return Systems(systemsRef());
}

void Log::setSystems(const Log::Systems &systems)
{
    QMutexLocker lock(systemsMutex());
    Q_UNUSED(lock);
    systemsRef() = systems;
}

void Log::setSystem(const Log::System &system, bool status)
{
    QMutexLocker lock(systemsMutex());
    Q_UNUSED(lock);
    systemsRef().setFlag(system,status);
}

void Log::messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Systems systems_ = systems();

    // console
    if (systems_.testFlag(ConsoleSystem)) {
        consoleMessageHandler(type,context,msg);
    }

    // syslog
    if (systems_.testFlag(SyslogSystem)) {
        if (syslogSupport()) {
            syslogMessageHandler(type,context,msg);
        } else {
            if (!systems_.testFlag(ConsoleSystem)) {
                consoleMessageHandler(type,context,msg);
            }
        }
    }

    // internal
    if (systems_.testFlag(InternalSystem)) {
        internalMessageHandler(type,context,msg);
    }

    if (type == QtFatalMsg) {
        abort();
    }
}

Logger *Log::logger()
{
    static Logger logger;
    return &logger;
}

Log::Systems &Log::systemsRef()
{
    static Systems systems;
    return systems;
}

QMutex *Log::systemsMutex()
{
    static QMutex mutex;
    return &mutex;
}

void Log::consoleMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static const char* type_str[] = {   "[   Debug] %s -> %s\n",
                                        "[ Warning] %s -> %s\n",
                                        "[Critical] %s -> %s\n",
                                        "[   Fatal] %s -> %s\n",
                                        "[    Info] %s -> %s\n" };

    QByteArray f(context.function);
    int pos = f.indexOf('(');
    f.remove(pos,f.length()-pos);
    pos = f.lastIndexOf(' ');
    f.remove(0,pos+1);

    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
        case QtDebugMsg:
            fprintf(OUTPUT_DEVICE, type_str[type], f.constData(), localMsg.constData());
            break;
        case QtInfoMsg:
            fprintf(OUTPUT_DEVICE, type_str[type], f.constData(), localMsg.constData());
            break;
        case QtWarningMsg:
            fprintf(OUTPUT_DEVICE, type_str[type], f.constData(), localMsg.constData());
            break;
        case QtCriticalMsg:
            fprintf(OUTPUT_DEVICE, type_str[type], f.constData(), localMsg.constData());
            break;
        case QtFatalMsg:
            fprintf(OUTPUT_DEVICE, type_str[type], f.constData(), localMsg.constData());
            //abort();
            break;
    }
    fflush(OUTPUT_DEVICE);
}

void Log::syslogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{    
    Q_UNUSED(type);
    Q_UNUSED(context);
    Q_UNUSED(msg);

#ifdef Q_OS_LINUX
    static const char* type_str[] = {   "[   Debug] %s -> %s",
                                        "[ Warning] %s -> %s",
                                        "[Critical] %s -> %s",
                                        "[   Fatal] %s -> %s",
                                        "[    Info] %s -> %s" };

    QByteArray f(context.function);
    int pos = f.indexOf('(');
    f.remove(pos,f.length()-pos);
    pos = f.lastIndexOf(' ');
    f.remove(0,pos+1);

    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
        case QtDebugMsg:
          syslog(LOG_DEBUG, type_str[type], f.constData(), localMsg.constData());
          break;
        case QtInfoMsg:
          syslog(LOG_INFO, type_str[type], f.constData(), localMsg.constData());
          break;
        case QtWarningMsg:
          syslog(LOG_WARNING, type_str[type], f.constData(), localMsg.constData());
          break;
        case QtCriticalMsg:
          syslog(LOG_CRIT, type_str[type], f.constData(), localMsg.constData());
          break;
        case QtFatalMsg:
          syslog(LOG_ALERT, type_str[type], f.constData(), localMsg.constData());
          //abort();
    }
#endif
}

void Log::internalMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    static const char* type_str[] = {   "[   Debug] %s -> %s",
                                        "[ Warning] %s -> %s",
                                        "[Critical] %s -> %s",
                                        "[   Fatal] %s -> %s",
                                        "[    Info] %s -> %s" };

    static const char* type_str_short[] = {   "[   Debug]",
                                              "[ Warning]",
                                              "[Critical]",
                                              "[   Fatal]",
                                              "[    Info]" };
    QByteArray f(context.function);
    int pos = f.indexOf('(');
    f.remove(pos,f.length()-pos);
    pos = f.lastIndexOf(' ');
    f.remove(0,pos+1);

    QByteArray localMsg = msg.toLocal8Bit();
    QString res_msg;
    switch (type) {
        case QtDebugMsg:
            res_msg = QString::asprintf(type_str[type], f.constData(), localMsg.constData());
            break;
        case QtInfoMsg:
            res_msg = QString::asprintf(type_str[type], f.constData(), localMsg.constData());
            break;
        case QtWarningMsg:
            res_msg = QString::asprintf(type_str[type], f.constData(), localMsg.constData());
            break;
        case QtCriticalMsg:
            res_msg = QString::asprintf(type_str[type], f.constData(), localMsg.constData());
            break;
        case QtFatalMsg:
            res_msg = QString::asprintf(type_str[type], f.constData(), localMsg.constData());
            //abort();
            break;
    }
    logger()->logReady(res_msg,type,type_str_short[type],context.function,localMsg);
}

