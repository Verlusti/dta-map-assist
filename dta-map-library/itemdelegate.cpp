#include "itemdelegate.h"

#include "item.h"

#include <QPainter>
#include <QApplication>
#include <QDebug>

ItemDelegate::ItemDelegate(QWidget *parent) : QStyledItemDelegate(parent)
{

}

ItemDelegate::~ItemDelegate()
{

}

void ItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_ASSERT(index.isValid());

    const QStandardItemModel* m = qobject_cast<const QStandardItemModel*>(index.model());
    Item* i = (m) ? dynamic_cast<Item*>(m->itemFromIndex(index)) : Q_NULLPTR;
    if (!m || !i) {
        QStyledItemDelegate::paint(painter,option,index);
        return;
    }

    QStyleOptionViewItem opt = option;
    initStyleOption(&opt, index);
    QStyle *style = opt.widget ? opt.widget->style() : QApplication::style();

    opt.text = i->displayText();

    style->drawControl(QStyle::CE_ItemViewItem, &opt, painter, opt.widget);
}

QSize ItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // TODO
    return QStyledItemDelegate::sizeHint(option,index);
}

QWidget *ItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    // TODO
    return QStyledItemDelegate::createEditor(parent,option,index);
}

void ItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    // TODO
    return QStyledItemDelegate::setEditorData(editor,index);
}

void ItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    // TODO
    QStyledItemDelegate::setModelData(editor,model,index);
}

void ItemDelegate::commitAndCloseEditor()
{

}

