#include "mapitemmodel.h"

#include "ini.h"

#include "item/sides.h"
#include "item/colors.h"

#include "item/mapdata.h"
#include "item/standarditem.h"
#include "item/sectionitem.h"

#include <QDebug>

MapItemModel::MapItemModel(QObject *parent) : QStandardItemModel(parent)
{
    init();
}

MapItemModel::~MapItemModel()
{
    qDeleteAll(m_ItemSyncMap);
}

bool MapItemModel::import(const Ini &ini)
{
    emit(importStarted());

    MapData* mapdata = mapData();
    if (!mapdata) {
        emit(importFinished(false));
        return false;
    }

    foreach (QByteArray section_name, ini.names()) {
        Ini::SectionData* section_data = ini.data(section_name);
        if (!section_data) continue;

        Item* reserved_section_item = mapdata->childWithReservedKey(section_name);
        if (reserved_section_item) {
            foreach (QByteArray key, section_data->keys()) {
                reserved_section_item->import(key,section_data->value(key));
            }
        } else {
            SectionItem* section_item = addChild<SectionItem>();
            section_item->setData(section_name,Item::KeyRole);

            foreach (QByteArray key, section_data->keys()) {
                section_item->import(key,section_data->value(key));
            }
        }
    }

    emit(importFinished(true));
    return true;
}

MapData *MapItemModel::mapData() const
{
    QModelIndex index = findIndex(QVariantList() << Item::MapDataItemType);
    if (!index.isValid()) return Q_NULLPTR;
    MapData* map_data_item = dynamic_cast<MapData*>(itemFromIndex(index));
    return map_data_item;
}

bool MapItemModel::addItemSync(Item *item)
{
    if (!item || item->itemSync() == Item::NoItemSync) return false;

    int sync = item->itemSync();
    QList<Item*>* sync_list = m_ItemSyncMap.value(sync,Q_NULLPTR);
    if (!sync_list) {
        sync_list = new QList<Item*>();
        m_ItemSyncMap.insert(sync,sync_list);
    }

    if (!sync_list->contains(item)) {
        sync_list->append(item);
    }

    return true;
}

void MapItemModel::removeItemSync(Item *item)
{
    foreach (QList<Item*>* sync_list, m_ItemSyncMap) {
        if (!sync_list) continue;
        if (sync_list->contains(item)) {
            sync_list->removeAll(item);
            return;
        }
    }
}

void MapItemModel::runItemSync(Item *item, const QVariant &value, int role)
{
    if (!item) return;

    int sync = item->itemSync();
    if (sync <= Item::NoItemSync) return;

    QList<Item*>* sync_list = m_ItemSyncMap.value(sync,Q_NULLPTR);
    if (!sync_list) return;

    foreach (Item* i, *sync_list) {
        if (i == item) continue;
        i->setSyncBlocked(true);
        i->sync(item,value,role);
        i->setSyncBlocked(false);
        //qDebug() << i;
    }
}

void MapItemModel::init()
{
    // global data
    addChild<Sides>();
    addChild<Colors>();

    // map data
    addChild<MapData>();
}

QModelIndex MapItemModel::findIndexPrivate(const QVariantList &keys, const int &level, const QModelIndex &next_index) const
{
    if (level >= keys.count()) return QModelIndex();
    QVariant key = keys.at(level);

    QModelIndex current_index = (level > 0) ? next_index : index(0,0);
    if (!current_index.isValid()) return QModelIndex();

    QModelIndexList list = match(current_index,Item::KeyRole,key,-1,Qt::MatchExactly);
    if (list.count() != 1) return QModelIndex();
    if (level + 1 == keys.count()) {
        return list.first();
    }

    return findIndexPrivate(keys,level+1,list.first().child(0,0));
}
