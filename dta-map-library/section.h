#ifndef SECTION_H
#define SECTION_H

#include "option.h"

#include <QDebug>
#include <QObject>
#include <QMap>
#include <QMetaEnum>

#include <QList>
#include <QStringList>
#include <QByteArrayList>
#include <QVariantList>

class QMutex;

class DTAMAPLIBRARYSHARED_EXPORT Section : public QObject
{
    Q_OBJECT
public:
    enum SectionType {
        NoType,
        StandardType,
//        DynamicType,
//        UnknownType
    };
    Q_ENUM(SectionType)

    enum KeyType {
        NoKey,
        ByteArrayKey,
        IntKey,
        EnumKey
    };
    Q_ENUM(KeyType)

    explicit Section(QObject *parent = Q_NULLPTR);
    virtual ~Section();

    static QByteArrayList registeredClasses();
    static Section* create(const QByteArray &section_name, QObject* parent = Q_NULLPTR);
    static int createStandardSections(QObject* parent);
    virtual Section* create(QObject* parent = Q_NULLPTR) const = 0;

    static SectionType sectionType(const QByteArray& section_name);
    virtual SectionType sectionType() const = 0; // override

    static KeyType keyType(const QByteArray& section_name);
    virtual KeyType keyType() const = 0; // override
    virtual Option::ValueType defaultValueType() const = 0; // override
    virtual QMetaObject defaultOptionMetaObject() const { return Option::staticMetaObject; } // override if necessary

    virtual void clear(); // override if necessary

    virtual bool fromIni(const QByteArray& ini_data); // override if necessary
    virtual QByteArray toIni() const; // override if necessary

    Option* option(const QByteArray& key);
    Option* option(const int& key);
    template <typename OptionT>
    QList<OptionT> options() { return findChildren<OptionT>(QString(),Qt::FindDirectChildrenOnly); }
    QVariant optionKey(Option* option) const { return (option) ? m_Options.key(option) : QVariant(); }

    bool connectWidget(QWidget* widget, Option* option);
    bool connectWidget(QWidget* widget, const QByteArray& option_key) { return connectWidget(widget,option(option_key)); }
    bool connectWidget(QWidget* widget, const int& option_key) { return connectWidget(widget,option(option_key)); }

signals:
    void cleared();
    void readFinished();

    void optionAdded(Option* option);

public slots:

protected:
    QMap<QVariant, Option*> m_Options;
    QByteArray m_BinaryData;

    virtual QMetaEnum metaEnum() const = 0;

    template <typename ValueT> ValueT fromVariant(const QVariant& value) const { return value.value<ValueT>(); }
    template <typename ValueT> QVariant toVariant(const ValueT& value) const { return QVariant::fromValue<ValueT>(value); }

    static void registerClass(Section* section);

    // options
    bool addOption(const QVariant& key, Option* option);
    bool addOption(const QVariant& key, const Option::ValueType& value_type, Tristate::OutputFormat tristate_output_format = Tristate::YesNo);
    bool addStandardOption(const QVariant& key, Option* option);
    bool addStandardOption(const QVariant& key, const Option::ValueType& value_type, Tristate::OutputFormat tristate_output_format = Tristate::YesNo);

    Option* createDefaultOption(const Option::ValueType& value_type);

private:
    static QMutex* registeredClassesMutex();
    static QMap<QByteArray, Section*>& registeredClassesMap();
};

// Specialisation for value conversion variant <-> bytearray list
template <> inline QByteArrayList Section::fromVariant(const QVariant& value) const { return value.toByteArray().split(','); }
template <> inline QVariant Section::toVariant(const QByteArrayList& value) const { return QVariant(value.join(',')); }

// Specialisation for value conversion variant <-> int list
template <> inline IntList Section::fromVariant(const QVariant& value) const { IntList list; foreach (QByteArray ba, value.toByteArray().split(',')) { ba.toInt(); } return list; }
template <> inline QVariant Section::toVariant(const IntList& value) const { QByteArrayList list; foreach (int i, value) { list.append(QByteArray::number(i)); } return QVariant(list.join(',')); }

// CRTP section class
template <typename T> class SectionT : public Section
{
public:
    struct Register {
        Register() {
            registerClass(qobject_cast<Section*>(new T()));
        }
    };

    explicit SectionT(QObject *parent = Q_NULLPTR) : Section(parent) { setObjectName(T::staticMetaObject.className()); }
    virtual ~SectionT() {}

    virtual Section* create(QObject* parent = Q_NULLPTR) const { return new T(parent); }

protected:
    virtual QMetaEnum metaEnum() const { return QMetaEnum::fromType<typename T::Options>(); }
};

#endif // SECTION_H
