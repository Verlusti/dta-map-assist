#ifndef DTA_H
#define DTA_H

#include "dta-map-library_global.h"

#include <QObject>
#include <QDir>
#include <QList>
#include <QPair>

class QMutex;

class DTAMAPLIBRARYSHARED_EXPORT Dta : public QObject
{
    Q_OBJECT
public:
    typedef void (*FP)();

    template <void (*F)()>
    struct UpdateFuntion
    {
        UpdateFuntion() { updaterListPrivate(F); }
    };

    enum File
    {
        DtaExe,
        RulesIni,
        GameOptionsIni
    };
    Q_ENUM(File)

    enum Directory
    {
        Base,
        Ini,
        Resources,
        Maps
    };
    Q_ENUM(Directory)

    enum Status
    {
        Ok,
        Empty,
        FolderBad,
        ExeMissing
    };
    Q_ENUM(Status)

private:
    explicit Dta(QObject *parent = Q_NULLPTR);
public:
    virtual ~Dta();

    static Dta* instance();

    static QString baseDirFromRegistry();

    static QDir directory(const Directory& directory_);
    static QFileInfo file(const File& file_);

    static void readSettings();
    static void writeSettings();

    static QDir baseDir();
    static void setBaseDir(const QDir& base_dir, bool no_update = false);
    static void setBaseDir(const QString& base_dir, bool no_update = false) { setBaseDir(QDir(base_dir),no_update); }
    static Status baseDirStatus() { return baseDirStatus(baseDir()); }
    static Status baseDirStatus(const QDir& base_dir);
    static Status baseDirStatus(const QString& base_dir) { return baseDirStatus(QDir(base_dir)); }
    static QString baseDirStatusString() { return statusString(baseDirStatus()); }
    static QString baseDirStatusString(const QDir& base_dir) { return statusString(baseDirStatus(base_dir)); }
    static QString baseDirStatusString(const QString& base_dir) { return statusString(baseDirStatus(base_dir)); }
    static bool isBaseDirValid() { return (baseDirStatus() == Ok); }
    static bool isBaseDirValid(const QDir& base_dir) { return (baseDirStatus(base_dir) == Ok); }
    static bool isBaseDirValid(const QString& base_dir) { return (baseDirStatus(base_dir) == Ok); }

    static QString statusString(const Status& status);

//    static QList<QPair<QByteArray, QByteArray> > readSectionFromIni(const QString& file_name, const QByteArray& section) { return readSectionFromIni(QFileInfo(file_name),section); }
//    static QList<QPair<QByteArray, QByteArray> > readSectionFromIni(const QFileInfo& file_info, const QByteArray& section);

signals:
    void baseDirChanged(const QString& base_dir);

public slots:

private:
    static QDir& baseDirPrivate();
    static QMutex* baseDirMutex();

    static QList<FP>& updaterListPrivate(FP new_fp = Q_NULLPTR);
    static QMutex* updaterListMutex();

    static void updateAll();
};

//#include <QObject>
//#include <QDir>
//#include <QFileInfo>

//class Dta : public QObject
//{
//    Q_OBJECT
//public:
//    explicit Dta();
//    virtual ~Dta();

//    static QString installDirFromRegistry();

//    QDir baseDir() const { return m_BaseDir; }
//    void setBaseDir(const QDir& base_dir) { m_BaseDir = base_dir; }
//    bool isBaseDirValid() const;
//    QString baseDirStatus() const { return staticBaseDirStatus(m_BaseDir.path()); }
//    static QString staticBaseDirStatus(const QString& base_dir_str);

//    QDir iniDir() const;
//    QDir mapsDir() const;
//    QDir coopMapsDir() const;
//    QDir customMapsDir() const;
//    QDir resourcesDir() const;

//    QFileInfo rulesIniFile() const;
//    QFileInfo gameOptionsIniFile() const;

//public slots:


//signals:

//public slots:

//private:
//    QDir m_BaseDir;
//};

#endif // DTA_H
