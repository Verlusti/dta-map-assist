#ifndef MAPITEMMODEL_H
#define MAPITEMMODEL_H

#include "item.h"

#include <QObject>
#include <QVariantList>
#include <QStandardItemModel>

class Ini;
class MapData;

class DTAMAPLIBRARYSHARED_EXPORT MapItemModel : public QStandardItemModel
{
    Q_OBJECT
public:
    explicit MapItemModel(QObject *parent = Q_NULLPTR);
private:
    explicit MapItemModel(int rows, int columns, QObject* parent = Q_NULLPTR) : QStandardItemModel(rows,columns,parent) {}
public:
    virtual ~MapItemModel();

    QModelIndex findIndex(const QVariantList& keys) const { return findIndexPrivate(keys); }
    Item* findItem(const QVariantList& keys) const { return dynamic_cast<Item*>(itemFromIndex(findIndex(keys))); }

    void addChild(Item* child_item) { if (!child_item) return; appendRow(child_item); child_item->init(); }
    template <typename ChildT> ChildT* addChild() { ChildT* child_item = new ChildT(); addChild(child_item); return child_item; }

    bool import(const Ini& ini);

signals:   
    void importStarted();
    void importFinished(bool success);

public slots:    

protected:
    QMap<int, QList<Item*>*> m_ItemSyncMap;

    MapData* mapData() const;

    bool addItemSync(Item* item);
    void removeItemSync(Item* item);
    void runItemSync(Item* item, const QVariant& value, int role);

private:
    void init();  

    QModelIndex findIndexPrivate(const QVariantList& keys, const int& level = 0, const QModelIndex& next_index = QModelIndex()) const;

    friend class Item;
};

#endif // MAPITEMMODEL_H
