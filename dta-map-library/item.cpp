#include "item.h"

#include "mapitemmodel.h"

#include <QMetaEnum>
#include <QDebug>

Item::~Item()
{
    MapItemModel* m = qobject_cast<MapItemModel*>(model());
    if (m) m->removeItemSync(this);
}

void Item::init()
{
    if (m_Init) return;
    m_Init = true;
    setup();
}

QString Item::displayText() const
{
    if (!hasValue()) return data(Qt::DisplayRole).toString();
    QString ret = data(Qt::DisplayRole).toString();
    ret.append(QString(QStringLiteral(" [%1]")).arg(data(ValueRole).toString()));
    return ret;
}

QVariant Item::data(int role) const
{
    if (role == Qt::EditRole) role = ValueRole;
    return QStandardItem::data(role);
}

void Item::setData(const QVariant &value, int role)
{
    if (role == Qt::EditRole) role = ValueRole;
    QStandardItem::setData(value,role);

    if (!m_SyncBlocked && itemSync() > NoItemSync) {
        MapItemModel* m = qobject_cast<MapItemModel*>(model());
        if (!m) return;
        m->runItemSync(this,value,role);
    }
}

void Item::addChild(Item *child_item)
{
    if (!child_item) return;
    appendRow(child_item);
    if (child_item->itemSync() != NoItemSync) {
        MapItemModel* m = qobject_cast<MapItemModel*>(model());
        if (m) m->addItemSync(child_item);
    }
    child_item->init();
}

QVariantList Item::keyPath() const
{
    QVariantList list;
    list.append(data(KeyRole));
    Item* parent_ = dynamic_cast<Item*>(parent());
    while (parent_) {
        list.prepend(parent_->data(KeyRole));
        parent_ = dynamic_cast<Item*>(parent_->parent());
    }
    return list;
}

Item *Item::childWithReservedKey(const QVariant &reserved_key) const
{
    QModelIndexList child_list = model()->match(index().child(0,0),ReservedKeyRole,reserved_key,1,Qt::MatchExactly);
    if (child_list.count() == 0) return Q_NULLPTR;
    return dynamic_cast<Item*>(model()->itemFromIndex(child_list.first()));
}

void Item::setDataEnumOptions(const QVariant &value, int role, const QMetaEnum &meta_enum, const QByteArray &class_name)
{
    if (role != KeyRole) {
        Item::setData(value,role);
        return;
    }

    bool ok = false;
    int id = value.toInt(&ok);
    if (!ok) id = -1;

    Item::setData(id,KeyRole);
    QByteArray name = meta_enum.valueToKey(id);
    if (!name.isEmpty()) Item::setData(name,ReservedKeyRole);
    if (name.isEmpty()) name = class_name;
    setText(name);
    setIcon(icon());
}
